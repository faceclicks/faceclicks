# Basics

### making websites from jupyter notebooks

https://nbsphinx.readthedocs.io/en/0.4.2/usage.html

# GitHub integration

### getting github to use custom style of website

https://stackoverflow.com/questions/35227274/how-to-port-python-sphinx-doc-to-github-pages/35227443#35227443

### tricking sphinx to put html output into root of docs folder

https://jefflirion.github.io/sphinx-github-pages.html

### making github-pages from docs folder

https://help.github.com/en/articles/configuring-a-publishing-source-for-github-pages#publishing-your-github-pages-site-from-a-docs-folder-on-your-master-branch

# Style over substance

### jupyter-book template

https://jupyterbook.org/intro.html
https://sphinx-book-theme.readthedocs.io/en/latest/
```pip install sphinx-book-theme```

### toggle code cells

https://nbsphinx.readthedocs.io/en/0.7.0/prolog-and-epilog.html
https://stackoverflow.com/questions/27934885/how-to-hide-code-from-cells-in-ipython-notebook-visualized-with-nbviewer/47017746#47017746

### make thumbnail table of contents

https://nbsphinx.readthedocs.io/en/0.7.0/subdir/gallery.html

* to choose a cell for the thumbnail:
    * use "View"->"Cell Toolbar"->"Tags"
    * and add the folling tag: nbsphinx-thumbnail
