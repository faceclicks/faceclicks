import numpy as np

def pixelate_rgb(img, window):
    """
    https://stackoverflow.com/a/63627054 CC BY-SA 4.0 Aray Karjauv 
    """
    n, m, _ = img.shape
    n, m = n - n % window, m - m % window
    img1 = np.zeros((n, m, 3))
    for x in range(0, n, window):
        for y in range(0, m, window):
            img1[x:x+window,y:y+window] = img[x:x+window,y:y+window].mean(axis=(0,1))
    return img1
    
    
def pixelate_bin(img, window, threshold):
    """
    https://stackoverflow.com/a/63627054 CC BY-SA 4.0 Aray Karjauv
    """
    n, m = img.shape
    n, m = n - n % window, m - m % window
    img1 = np.zeros((n,m))
    for x in range(0, n, window):
        for y in range(0, m, window):
            if img[x:x+window,y:y+window].mean() > threshold:
                img1[x:x+window,y:y+window] = 1
    return img1

