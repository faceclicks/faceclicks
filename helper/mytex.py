import re

def make_tex(var_name, value):
    """add variable for LaTeX use
    
    Input:
    ------
    var_name: string
        cannot have underscores!, best use camelStyle
    
    Output:
    -------
    Nothing, writes to file
    
    """
    
    # line of new variable and its value in latex format
    newline = f'\\newcommand\\{var_name}{{{value}}}\n'
    
    # find variable name in each line
    regex = re.compile(r'\\newcommand\\(\w+)\{.*')
    
    # each time, we re-write the whole file, so subsequently
    # we decide which old lines get to go into the new file and
    # which will be replaced
    newlines = []
    
    # get the file in the current version
    with open('../reports/ms/variables.tex', 'r')as f:
        
        # get all lines into a list
        lines = f.readlines()
        
        # check each line
        for line in lines:
            # see if the variable that we want to write now is already
            # in one of the lines
            try:
                this_var_name = regex.findall(line)[-1]
                
                # if this line does not contain the variable we want to change,
                # we keep it
                if this_var_name != var_name:
                    newlines.append(line)
                    
                # if this is the line we want to change, we do not keep it,
                # but we make a note which line it is
                else:
                    overwrite_line = line
            
            # the regex will not work if there is no match
            except:
                # this is expected for the last line which should be a blank
                if line == '\n':
                    pass
                # otherwise, we want to be alterted that we have wrongly formatted lines
                else:
                    print(f'WARNING: line "{line}" has wrong format!')

    # in addition to the unchanged lines, we add the newline and a final backspace
    newlines.append(newline)
    newlines.append('\n')
    
    # if we changed something, show what we changed
    try:
        print(f'overwriting "{overwrite_line}"!')
    except:
        pass
    print(f'new line: "{newline}"')
    
    # write all old and new lines, as collected in the newline list
    with open('../reports/ms/variables.tex','w') as f:
        f.writelines(newlines)
        
    return