#!/usr/bin/env python
# coding: utf-8

# # Dissimilarity Analyses
# 
# Correlate Answer/Feature Patterns, Make Dissimiliarity Matrices, MDS-Visualizations and Representation Similiarity Analyses

# ### import modules

# In[1]:


import json
import pickle

import numpy as np
import pandas as pd

from sklearn import preprocessing, manifold

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')


# In[2]:


import sys
sys.path.append('../helper/')
from procrustes import procrustes
from pixelate_rgb import pixelate_rgb


# ### get settings

# In[3]:


with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)
    
my_order = settings_dict['my_order']

with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)


# Reverse order for dissimilarity, where lower values are "better"

# In[4]:


reverse_palette = color_palette[::-1]


# ## For Answers

# ### get data

# In[5]:


data_df = pd.read_csv('../data/interim/clean_big_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])
ans_df = data_df.xs('Answer', level=1, axis=1)
ans_df.tail()


# ### accuracies for all different emo-identity cases

# In[6]:


def make_similarity_df(cond, ans_df):
    
    cond_df = ans_df.loc[:, cond]

    d = {}

    for col1 in cond_df.columns:
        for col2 in cond_df.columns:

            # get emo-label and identity for both selected cases
            emo1, ident1 = col1
            emo2, ident2 = col2

            # compare the answer pattern of two emo-ident cases
            d[(emo1, ident1, emo2, ident2)] = (cond_df.loc[:, col1] == cond_df.loc[:, col2]).mean()

    df = pd.DataFrame(d, index = ['prop'])
    df = df.stack([0, 1]).loc['prop', :]
    
    return df


# The similarity_df codes how much overlap there is in the 202 answers given for an image and another image, so it is bound between 0 and 1

# In[7]:


similarity_df = make_similarity_df('Full', ans_df)


# ### make all conditions

# In[8]:


diss_ans_dict = {}

conds = ans_df.columns.levels[0]

for n, cond in enumerate(conds):
    
    similarity_df = make_similarity_df(cond, ans_df)
    similarity_df = similarity_df.loc[my_order, my_order]
    diss_df = 1 - similarity_df

    diss_ans_dict[cond] = diss_df


# ### visualize heatmaps

# In[9]:


def make_diss_ax(diss_df, ax, n):
    
    ax = plt.subplot(1, len(conds), n+1)
    sns.heatmap(diss_df, square=True, vmin=0, vmax=1, cmap=reverse_palette, cbar=False, ax=ax)
    
    if n > 0:
        ax.set_yticks([])
    ax.set_xlabel(''); ax.set_ylabel('')
    ax.set_title(cond, fontsize=36)
    
    # Custom x and y ticks
    
    pstn = diss_df.reset_index().index + 0.5
    lbls = [i[1] for i in diss_df.index]
    my_fntsz = 10
    
    ax.set_xticks(pstn); ax.set_yticks(pstn)
    ax.set_xticklabels(lbls, fontsize=my_fntsz); ax.set_yticklabels(lbls, fontsize=my_fntsz)

    # Custom x and y labels
    
    x_str = ''
    n_empty = 28

    for i in my_order:
        x_str += (str(i) + ' '*n_empty)[:n_empty]
    
    x_str = x_str.strip()
    
    ax.set_xlabel(x_str, y=-1, fontsize=my_fntsz)

    if n > 0:
        ax.set_yticks([])
        
    if n == 0:
        y_str = ''
        
        for i in my_order[::-1]:
            y_str += (str(i) + ' '*n_empty)[:n_empty]       
        
        y_str = y_str.strip()
        ax.set_ylabel(y_str, x=-1, fontsize=my_fntsz)

        
    for step in range(0, 71, 10):
        ax.axhline(step, color='white')
        ax.axvline(step, color='white')

    return ax    


# In[10]:


fig = plt.figure(figsize=(32, 16), dpi=300)

for n, cond in enumerate(diss_ans_dict.keys()):
    
    diss_df = diss_ans_dict[cond]
    ax = plt.subplot(1, 3, n+1)
    ax = make_diss_ax(diss_df, ax, n)
    
plt.tight_layout()
plt.savefig('../reports/figures/diss_ans_fig.png', bbox_inches='tight')
plt.show()


# ### make MDS

# In[11]:


mds = manifold.MDS(max_iter=1000000, dissimilarity='precomputed')


# In[12]:


def make_mds_dict(diss_dict):

    mds_dict = {}

    for n, cond in enumerate(diss_dict.keys()):

        diss_df = diss_dict[cond]

        mds_coords = mds.fit_transform(diss_df.values)
        mds_dict[cond] = pd.DataFrame(mds_coords, diss_df.index)
        
    return mds_dict


# In[13]:


mds_dict = make_mds_dict(diss_ans_dict)


# ### reference orientation

# In[14]:


my_reference = mds_dict['Full'].values


# ### visualize MDS

# In[15]:


def make_ax(mds_df, ax, my_reference=my_reference):

    # rotate to match template
    d, Z, tform = procrustes( my_reference, mds_df.values, scaling=False)
    new_mds_df = pd.DataFrame(Z, index=mds_df.index, columns=mds_df.columns)

    for emo, ident in new_mds_df.index:

        n = np.where(np.array(my_order)==emo)[0][0]
        x, y = new_mds_df.loc[(emo, ident), :]

        plt.plot(x, y, 'o', color=palette7[n], markersize=32, alpha=0.5, label=emo)
        #plt.annotate(xy=(x, y), text=ident)

    mean_mds_df = new_mds_df.groupby(level=0).mean()
    for i in mean_mds_df.index:
        x, y = mean_mds_df.loc[i, :]
        txt = i.upper()[:3]
        ax.annotate(text=txt, xy=(x, y), fontsize=32)

    sns.despine(left=True, bottom=True)

    ax.set_xticks([]); ax.set_yticks([])

    ax.set_title(cond, fontsize=48, y=1.05)
        
    return ax


# In[16]:


fig = plt.figure(figsize=(24, 8), dpi=300)

for n, cond in enumerate(mds_dict.keys()):

    ax = plt.subplot(1, 3, n+1)
    mds_df = mds_dict[cond]
    ax = make_ax(mds_df, ax)

plt.tight_layout()
plt.savefig('../reports/figures/mds_ans_fig.png', bbox_inches='tight')
plt.show()


# ## For Click Patterns

# ### Dissimilarities

# In[17]:


def make_diss_df(full_df):
    
    corr_df = full_df.T.corr()
    diss_df = 1 - corr_df
    
    return diss_df


# In[18]:


diss_click_dict = {}

for n, cond in enumerate(conds):

    with open(f'../data/interim/biggest_{cond}_flat_df.pkl', 'rb') as fp:
        biggest_flat_df = pickle.load(fp)

    cond_df = biggest_flat_df.groupby(level=[0, 2]).mean()
    diss_df = make_diss_df(cond_df)
    
    diss_click_dict[cond] = diss_df


# In[19]:


fig = plt.figure(figsize=(32, 16), dpi=300)

for n, cond in enumerate(diss_click_dict.keys()):
    
    diss_df = diss_click_dict[cond]
    ax = plt.subplot(1, 3, n+1)
    ax = make_diss_ax(diss_df, ax, n)
    
plt.tight_layout()
plt.savefig('../reports/figures/diss_click_fig.png', bbox_inches='tight')
plt.show()


# ### MDS

# In[20]:


mds_dict = make_mds_dict(diss_click_dict)


# ### Visualize

# In[21]:


fig = plt.figure(figsize=(24, 8), dpi=300)

for n, cond in enumerate(mds_dict.keys()):

    ax = plt.subplot(1, 3, n+1)
    mds_df = mds_dict[cond]
    ax = make_ax(mds_df, ax)

plt.tight_layout()
plt.savefig('../reports/figures/mds_click_fig.png', bbox_inches='tight')
plt.show()


# ## For Pixel Values

# ### get Data

# In[22]:


masked_img_df = pd.read_csv('../data/interim/img_masked_df.tsv', sep='\t', index_col=[0, 1, 2])
masked_img_df


# Demean

# In[23]:


masked_img_df = masked_img_df - masked_img_df.mean()


# ### Dissimilarities

# In[24]:


diss_im_dict = {}

for n, cond in enumerate(conds):

    cond_df = masked_img_df.loc[cond, :]
    diss_df = make_diss_df(cond_df)
    
    diss_im_dict[cond] = diss_df


# In[25]:


fig = plt.figure(figsize=(32, 16), dpi=300)

for n, cond in enumerate(diss_im_dict.keys()):
    
    diss_df = diss_im_dict[cond]
    ax = plt.subplot(1, 3, n+1)
    ax = make_diss_ax(diss_df, ax, n)
    
plt.tight_layout()
plt.savefig('../reports/figures/diss_im_fig.png', bbox_inches='tight')
plt.show()


# ### MDS

# In[26]:


mds_dict = make_mds_dict(diss_im_dict)


# ### Visualize

# In[27]:


fig = plt.figure(figsize=(24, 8), dpi=300)

for n, cond in enumerate(mds_dict.keys()):

    ax = plt.subplot(1, 3, n+1)
    mds_df = mds_dict[cond]
    ax = make_ax(mds_df, ax)

plt.tight_layout()
plt.savefig('../reports/figures/mds_im_fig.png', bbox_inches='tight')
plt.show()


# ## RSA
# 
# RSA compares the similarity of the similarities...

# In[28]:


dict_of_dicts = {'ans': diss_ans_dict,
                 'im': diss_im_dict,
                 'click': diss_click_dict}


# In[29]:


df = pd.DataFrame()

for d in dict_of_dicts:
    
    this_dict = dict_of_dicts[d]
    
    for key in this_dict.keys():
        this_df = this_dict[key]
        this_flat_df = pd.DataFrame( this_df.stack([0, 1]) ).T
        this_flat_df.index = pd.MultiIndex.from_tuples([[d, key]])
        
        df = pd.concat([df, this_flat_df])


# In[30]:


(df.max().max()).round()


# In[31]:


(df.min().min()).round()


# In[32]:


rsa_df = df.T.corr()


# In[33]:


plt.figure(figsize=(12, 12), dpi=100)
sns.heatmap(rsa_df, annot=True, square=True, vmin=-1, vmax=1, cmap='RdYlBu_r')
plt.show()


# The click patterns are closer to the answers than to the image pixel values

# ## With images

# In[34]:


stim_df = pd.read_csv('../data/interim/stim_df.tsv', sep='\t', index_col=[0, 1, 2])
stim_df.tail()


# In[35]:


from matplotlib.offsetbox import OffsetImage, AnnotationBbox
from collections import OrderedDict


# In[36]:


def make_pic_ax(mds_df, ax):

    fig, ax = plt.subplots(1, 1, figsize=(24, 24), dpi=300)

    for emo, ident in mds_df.index:

        n = np.where(np.array(my_order)==emo)[0][0]
        x, y = mds_df.loc[(emo, ident), :]

        ax.plot(x, y, 'o', color=palette7[n], label=emo)

        stim = stim_df.loc[('Full', emo, ident), 'file_name']

        arr_img = plt.imread(stim)
        arr_img = pixelate_rgb(arr_img/255., 60)


        imagebox = OffsetImage(arr_img, zoom=0.1)
        imagebox.image.axes = ax

        ab = AnnotationBbox(imagebox, (x, y), pad=0, 
                            bboxprops={'edgecolor':palette7[n], 'linewidth':10})

        ax.add_artist(ab)

    sns.despine(left=True, bottom=True)

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = OrderedDict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), loc=(1.1, 0.4))

    ax.set_xticks([]); ax.set_yticks([])

    return ax


# In[37]:


mds_dict = make_mds_dict(diss_click_dict)


# In[38]:


fig = plt.figure(figsize=(16, 6))

for cond in mds_dict.keys():
    
    mds_df = mds_dict[cond]
    ax = make_pic_ax(mds_df, ax)
    ax.set_title(cond, fontsize=24)

plt.show()


# ## Label identities

# Here we see that the pixel values cluster the images more by identity than by emotion; With the participant's answers it's the other way around

# In[39]:


for name, d in zip(['ans', 'click','im'], [diss_ans_dict, diss_click_dict, diss_im_dict]):
    
    mds_dict = make_mds_dict(d)
    
    for cond in mds_dict.keys():

        mds_df = mds_dict[cond]

        plt.figure(figsize=(6, 6))

        for emo, ident in mds_df.index:

            n = np.where(np.array(my_order)==emo)[0][0]
            x, y = mds_df.loc[(emo, ident), :]

            plt.plot(x, y, 'o', color=palette7[n], label=emo)
            #plt.annotate(xy=(x, y), text=ident)

        mean_df = mds_df.groupby(level=1).mean()
        for ident in mean_df.index:
            x, y = mean_df.loc[ident, :]
            plt.annotate(xy=(x, y), text=ident, fontsize=32)

        sns.despine(left=True, bottom=True)

        handles, labels = plt.gca().get_legend_handles_labels()
        by_label = OrderedDict(zip(labels, handles))
        plt.legend(by_label.values(), by_label.keys(), loc=(1.1, 0.4))

        plt.xticks([]); plt.yticks([])
        plt.title(name + ' ' + cond)
        plt.show()

