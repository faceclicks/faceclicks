#!/usr/bin/env python
# coding: utf-8

# # Compute Inferential Statistics

# ### import modules

# In[1]:


import json
import pickle

import numpy as np
import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from matplotlib import animation
from IPython.display import HTML

from matplotlib.offsetbox import OffsetImage, AnnotationBbox


# ### load settings

# In[2]:


with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)

with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# ## Get Data

# ### get click data

# In[3]:


click_diff_correct_df = pd.read_csv('../data/processed/click_diff_correct_df.tsv', sep='\t', index_col=[0])
click_diff_correct_df.tail()


# #### Reshape into Wide Format

# In[4]:


click_diff_correct_df = click_diff_correct_df.loc[:, my_order]
click_diff_correct_df.tail()


# #### What to do if no correct answer was given?
# 
# Decision here: replace with zero

# In[5]:


click_diff_correct_df = click_diff_correct_df.fillna(0)


# ### get answer count data

# In[6]:


ans_df = pd.read_csv('../data/processed/big_correct_df.tsv', sep='\t', index_col=[0], header=[0, 1])


# #### get Upper-Lower difference in Answers

# In[7]:


ans_diff_df = ans_df['Upper'] - ans_df['Lower']
ans_diff_df = ans_diff_df.loc[:, my_order]
ans_diff_df.tail()


# ## Inferential Statistics

# ### make statistics

# In[8]:


def make_stat(diff):
    
    t, tp = stats.ttest_1samp(diff, 0, alternative='two-sided')
    w, wp = stats.wilcoxon(diff, alternative='two-sided', zero_method='zsplit')

    d = {'t': t,
         'tp': tp,
         'w': w,
         'wp': wp,
        }
    
    return d


# ### threshold raw data according to p-value

# In[9]:


def make_thresh(c1, c2, stat_df, thresh):
    
    this_df = stat_df.loc[(c1, c2)].T
    
    descriptive = this_df.loc[:, 'mean diff']
    
    inferential = abs(this_df.loc[:, 'tp'])
    mask = inferential < thresh
    
    descriptive_masked = descriptive * mask
    descriptive_masked = descriptive_masked.replace(0, np.nan).values

    return descriptive_masked


# ### make all pairwise tests

# In[10]:


def make_paired_test(click_diff_correct_df):
    
    d = {}
    
    for c1 in my_order:
        
        for c2 in my_order:
            
            this_set = set([c1, c2])
            diff = click_diff_correct_df[c1] - click_diff_correct_df[c2]

            this_d = make_stat(diff)
            this_d['mean diff'] = diff.mean()

            d[(c1, c2)] = this_d
    
    stats_df = pd.DataFrame(d).T
    stats_df = stats_df.sort_index()
    
    return stats_df


# ## Stats for Clicks

# In[11]:


stats_df = make_paired_test(click_diff_correct_df)
stats_df.tail()


# ### mask data based on significance

# In[12]:


def make_masked_stats(stats_df, settings_dict=settings_dict):
    
    # get significance 
    my_sig = stats_df.loc[:, 'tp'].fillna(99)<settings_dict['p']
    
    # get raw data, but only for significant values!
    # this removes the non-sig from the dataframe
    only_sig_diff = stats_df.loc[my_sig, 'mean diff']
    
    # reordering the reduced data into an nxn matrix replaces the non-sig values by nan
    my_matrix = only_sig_diff.unstack()

    # order according to effects
    this_order = only_sig_diff.groupby(level=0).mean().sort_values().index[::-1]
    ordered_matrix = my_matrix.loc[this_order, this_order[::-1]]

    return ordered_matrix


# In[13]:


ordered_matrix = make_masked_stats(stats_df)
ordered_matrix


# In[14]:


sns.heatmap(ordered_matrix, square=True)


# ### make mask for upper left triangle

# In[15]:


def make_triangle_mask(matrix):
    
    mask = np.zeros_like(matrix)
    mask[np.triu_indices_from(mask)] = True
    mask = mask[::-1]
    
    return mask


# In[16]:


mask = make_triangle_mask(ordered_matrix)
mask


# In[17]:


sns.heatmap(ordered_matrix, mask=mask ,square=True)


# ### order full data into n x n matrix

# In[18]:


my_order = ordered_matrix.index


# In[19]:


full_matrix = stats_df.loc[:, 'mean diff'].unstack()
full_matrix = full_matrix.loc[my_order, my_order[::-1]]
full_matrix


# In[20]:


# only non-sig values
clean_full_matrix = (full_matrix*(ordered_matrix!=ordered_matrix)).replace(0, np.nan)


# ### make heatmap

# In[21]:


def make_plot(full_matrix, ordered_matrix, ax, color_palette=color_palette):

    my_max = abs(ordered_matrix).max().max()

    sns.heatmap(full_matrix, 
                mask=mask, 
                annot=True, 
                fmt='.0%', 
                square=True, 
                cmap='Greys', 
                vmin=-my_max, 
                vmax=my_max, 
                cbar=False,
                ax=ax)

    sns.heatmap(ordered_matrix, 
                mask=mask, 
                annot=True, 
                fmt='.0%', 
                square=True, 
                cmap=color_palette, 
                vmin=-my_max, 
                vmax=my_max, 
                cbar=False,
                ax=ax)

    ax.set_xlim(0,6,1)
    ax.set_ylim(6,0,-1)

    return ax


# In[22]:


plt.figure(figsize=(6, 6), dpi=100)

ax = plt.subplot(1, 1, 1)
ax = make_plot(clean_full_matrix, ordered_matrix, ax)

ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=18)
ax.set_yticklabels([i.get_text().upper()[:3] for i in ax.get_yticklabels()], fontsize=18)

ax.tick_params(bottom=False, left=False)

plt.savefig('../reports/figures/click_stat_fig.png', bbox_inches='tight', dpi=300)

plt.show()


# ## For Click Numbers

# In[23]:


stats_df = make_paired_test(ans_diff_df)

ordered_matrix = make_masked_stats(stats_df)
ordered_matrix = ordered_matrix.loc[my_order, my_order[::-1]] # adjust to click order

full_matrix = stats_df.loc[:, 'mean diff'].unstack()
full_matrix = full_matrix.loc[my_order, my_order[::-1]]
clean_full_matrix = (full_matrix*(ordered_matrix!=ordered_matrix)).replace(0, np.nan)


# In[24]:


plt.figure(figsize=(6, 6), dpi=100)

ax = plt.subplot(1, 1, 1)
ax = make_plot(clean_full_matrix, ordered_matrix, ax)

ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=18)
ax.set_yticklabels([i.get_text().upper()[:3] for i in ax.get_yticklabels()], fontsize=18)

ax.tick_params(bottom=False, left=False)

plt.savefig('../reports/figures/ans_stat_fig.png', bbox_inches='tight', dpi=300)
plt.show()

