#!/usr/bin/env python
# coding: utf-8

# # Interactive Plots

# ### import modules

# In[1]:


import pickle
import json

import numpy as np
import pandas as pd

from sparklines import sparklines

import skimage.io as sio

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns

sns.set_context('talk')

import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots

import panel as pn
pn.extension()


# In[2]:


import sys
sys.path.append('../helper')

from mytex import make_tex


# ### settings

# In[3]:


with open('../data/interim/im_dims.json', 'r') as fp:
    dim_dict = json.load(fp)
    
new_n_rows = dim_dict['new_n_rows']
new_n_cols = dim_dict['new_n_cols']

with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)

with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)
    
with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)
    
my_order = settings_dict['my_order']


# In[4]:


biggest_im = np.loadtxt('../data/interim/biggest_im.txt')


# ### Data

# In[5]:


data_df = pd.read_csv('../data/processed/big_correct_df.tsv', sep='\t', index_col=[0], header=[0, 1])
data_df.tail()


# ### Format DataFrame
# 
# Here, we put mean and standard deviation as strings into one cell

# In[6]:


str_df = data_df.mean().round(2).astype(str) + ' (' + data_df.std().round(3).astype(str) + ')'
str_df


# ### Sparkline Experiments
# 
# This is an experiment to embed histograms as text into pandas dataframes, which can then be embedded in LaTeX
# 
# compare: https://pbpython.com/styling-pandas.html

# In[7]:


def sparkline_str(x, mybins=11, myrange=(0,1)):
    '''
    from: https://pbpython.com/styling-pandas.html
    '''
    
    bins=np.histogram(x, bins=mybins, range=myrange)[0]
    sl = ''.join(sparklines(bins))
    return sl

sparkline_str.__name__ = "sparkline"


# In[8]:


sparkline_df = pd.concat([str_df, data_df.apply(lambda x:sparkline_str(x))], axis=1)
sparkline_df.columns = ['M (SD)', 'hist']
sparkline_df = sparkline_df.unstack(0)
sparkline_df = sparkline_df.reorder_levels([1, 0], axis=1)
sparkline_df = sparkline_df.sort_index(axis=1)


# In[9]:


sparkline_df


# For latex

# In[10]:


mytable = sparkline_df.style.format(precision=2).to_latex()


# Put as variable into latex

# In[11]:


#make_tex('statstable', mytable)


# ## Interactive Plots

# ### get data

# In[12]:


with open('../data/processed/stat_ims.json', 'rb') as fp:
    results_dict = pickle.load(fp)
    
with open('../data/processed/unthresh_stat_ims.json', 'rb') as fp:
    unthresh_results_dict = pickle.load(fp)


# ### make masked image for one condition

# In[13]:


df = pd.read_csv('../data/processed/stat_Full_baseline_df.tsv', sep='\t', index_col=[0, 1, 2])
df


# In[14]:


def make_Z(emo1, emo2, comparison, cond):
    
    Z_unthresh = unthresh_results_dict[cond][comparison][f'{emo1}-{emo2}']
    Z = results_dict[cond][comparison][f'{emo1}-{emo2}']
    
    return Z_unthresh, Z


# In[15]:


Z_unthresh, Z = make_Z('Surprised', 'Surprised', 'clean', 'Full')


# In[16]:


plt.imshow(Z)


# ### example static plot

# In[17]:


X, Y = np.meshgrid(range(new_n_cols), range(new_n_rows))
X.shape, Y.shape, Z.shape


# In[18]:


fig = plt.figure()
ax = plt.axes(projection="3d" )
ax.plot_surface(X, Y, Z, cmap =ListedColormap(color_palette))
plt.show()


# ### make color palette for interactive plots
# 
# Somehow this requires a palette with fewer entries (hence ```[::]```) and the colors have to be numbered

# In[19]:


small_palette = color_palette[::5]
paletteX = []
for n, i in enumerate(small_palette):
    paletteX.append([np.round(n/(len(small_palette)-1), 3), f'rgb({np.round(i[0],3)},{np.round(i[1],3)},{np.round(i[2],3)})' ])


# In[20]:


paletteX[:5]


# ### get image as background

# In[21]:


biggest_im = np.loadtxt('../data/interim/biggest_im.txt')


# ### interactive heatmap with plotly

# In[ ]:


fig = make_subplots()

fig.add_trace(go.Heatmap(z=np.flipud(biggest_im), 
                         xgap=0, 
                         ygap=0, 
                         coloraxis ="coloraxis1",
                         hoverinfo='skip',
                        ))

fig.add_trace(go.Heatmap(z=np.flipud(Z_unthresh), 
                         xgap=0, 
                         ygap=0, 
                         coloraxis="coloraxis2",
                         hoverinfo='z',
                         hovertemplate='Mean: %{z:.2f}<extra></extra>', #https://plotly.com/python/hover-text-and-formatting/
                         
                        ))

bin_thresh_Z = (abs(Z)>0).astype(float)
fig.add_trace(go.Contour(z=np.flipud(bin_thresh_Z),
                         contours_coloring='lines',
                         line_width=5,
                         colorscale='Greys_r',
                         showscale=False,
                         hoverinfo='skip',
                         contours=dict(start=0, end=1, size=1)
                        ))

fig.update_layout(height=new_n_rows*3, width=new_n_cols*3 + new_n_cols, 
                  coloraxis1 = {'colorscale':'Greys_r', 'showscale':False},
                  coloraxis2 = {'colorscale':paletteX, 'cmin':-.5, 'cmax':.5}, 
                 )
fig.show()


# ### 3D-Plot with Value on Z-Axis

# In[23]:


zero_map = (pd.DataFrame(Z).replace(np.nan,0)*0).values


# In[24]:


fig = go.Figure(go.Surface(z=Z_unthresh,
                           colorscale='Greens',
                           showscale=False))

fig.add_surface(z=Z, 
                colorscale=paletteX)

fig.add_surface(z=zero_map, 
                surfacecolor=biggest_im, 
                colorscale='Greys_r', 
                showscale=False)

fig.update_layout(scene=dict(zaxis=dict(range=[-0.5, 0.5]) ) )
fig.write_html("../reports/figures/face_3d.html")
fig.show()


# In[25]:


fig = make_subplots(rows=1, cols=7, 
                    subplot_titles=my_order, 
                    shared_yaxes=True)

for n, emo in enumerate(my_order):

    _ , Z = make_Z(emo, emo, 'clean', 'Full')

    fig.add_trace(go.Heatmap(z=np.flipud(biggest_im), 
                         xgap=0, 
                         ygap=0, 
                         coloraxis = "coloraxis1",
                         hoverinfo='skip',
                        ), 
              row=1, col=n+1)
    
    fig.add_trace(go.Heatmap(z=np.flipud(Z), 
                         xgap=0, 
                         ygap=0, 
                         coloraxis = "coloraxis2",
                         hoverinfo='z', #name='mean',
                        ), 
              row=1, col=n+1)

fig.update_layout(height=310, width=1000, 
                  coloraxis1 = {'colorscale':'Greys_r', 'showscale':False},
                  coloraxis2 = {'colorscale':paletteX, 'cmin':-.5, 'cmax':.5}, 
                  )
fig.write_html("../reports/figures/face_px.html")
fig.show()


# In[ ]:


fig = make_subplots(rows=7, cols=7, 
                    horizontal_spacing=0, vertical_spacing=0,
                    row_titles=my_order, column_titles=my_order,
                    shared_xaxes=True, shared_yaxes=True)

for m, emo1 in enumerate(my_order):
    
    for n, emo2 in enumerate(my_order):
        
        Z_unthresh , Z = make_Z(emo1, emo2, 'clean', 'Full')

        fig.add_trace(go.Heatmap(z=np.flipud(biggest_im), 
                             xgap=0, 
                             ygap=0, 
                             coloraxis = "coloraxis1",
                             hoverinfo='skip',
                            ), 
                  row=m+1, col=n+1)

        
        fig.add_trace(go.Heatmap(z=np.flipud(Z_unthresh), 
                             xgap=0, 
                             ygap=0, 
                             coloraxis = "coloraxis2",
                             hoverinfo='z',
                             hovertemplate='Mean: %{z:.2f}<extra></extra>', #https://plotly.com/python/hover-text-and-formatting/
                            ), 
                  row=m+1, col=n+1)

        bin_thresh_Z = (abs(Z)>0).astype(float)
        fig.add_trace(go.Contour(z=np.flipud(bin_thresh_Z),
                                 contours_coloring='lines',
                                 line_width=2,
                                 colorscale='Greys_r',
                                 showscale=False,
                                 contours=dict(start=0, end=1, size=1),
                                 hoverinfo='skip',
                                ),
                  row=m+1, col=n+1)

fig.update_layout(height=1200, width=1000, 
                  coloraxis1 = {'colorscale':'Greys_r', 'showscale':False},
                  coloraxis2 = {'colorscale':paletteX, 'cmin':-.5, 'cmax':.5}, 
                  )
fig.write_html("../reports/figures/face_conf_px.html")
fig.show()


# ### 3D

# In[ ]:


fig = make_subplots(
    rows=1, 
    cols=len(my_order), 
    subplot_titles=my_order, 
    shared_yaxes=True,
    specs=[[{'type': 'surface'}, 
            {'type': 'surface'}, 
            {'type': 'surface'}, 
            {'type': 'surface'}, 
            {'type': 'surface'}, 
            {'type': 'surface'}, 
            {'type': 'surface'}]] )

for n, emo in enumerate(my_order):

    Z_unthresh , Z = make_Z(emo, emo, 'clean', 'Full')
    fig.add_trace(go.Surface(z=zero_map, surfacecolor=biggest_im, coloraxis = "coloraxis1"), row=1, col=n+1)
    fig.add_trace(go.Surface(z=Z_unthresh, coloraxis = "coloraxis2"), row=1, col=n+1)
    fig.add_trace(go.Surface(z=Z, coloraxis = "coloraxis3"), row=1, col=n+1)

fig.update_layout(height=330, width=1000, 
                      coloraxis1 = {'colorscale':'Greys_r', 'showscale':False},
                      coloraxis2 = {'colorscale':'Greys', 'showscale':False}, 
                      coloraxis3 = {'colorscale':paletteX, 'cmin':-.5, 'cmax':.5}, 
                      #cene = dict(zaxis=dict(range=[-0.5, 0.5])),
                      )

fig.write_html("../reports/figures/face_px3d.html")
fig.show()


# ## Interactive Heatmaps With Swarms of individual Data Points

# ### get raw data

# In[28]:


with open('../data/interim/biggest_Full_flat_df.pkl', 'rb') as fp:
    raw_df = pickle.load(fp)
    
raw_df = raw_df.groupby(level=[0, 1, 3]).mean()

# add coordinates to index
a = []
for i in range(new_n_rows):
    for j in range(new_n_cols):
        a.append((i,j))
raw_df.columns = a


# In[29]:


raw_clean_df = pd.DataFrame()
mean_df = raw_df.groupby(level=[2]).mean()
for emo in raw_df.index.levels[0]:
    this_df = raw_df.loc[(emo, emo), :] - mean_df
    this_df.index = pd.MultiIndex.from_tuples([(emo, i) for i in this_df.index])
    raw_clean_df = pd.concat([raw_clean_df, this_df])


# In[30]:


def my_plot(x, y, comparison):

    fig = plt.figure(figsize=(14, 5))
    
    for n, emo in enumerate(my_order):
    
        if comparison == 'clean':
            full_data_df = raw_clean_df.loc[emo, :]
        elif comparison == 'baseline':
            full_data_df = raw_df.loc[emo, :]
        else:
            print('Wrong Condition!')
        
        _, Z = make_Z(emo, emo, comparison, 'Full')

        ax1 = plt.subplot(2,7,n+1)
        ax1.imshow(biggest_im, cmap='Greys_r')
        ax1.imshow(Z, vmin=-.7, vmax=.7, cmap=ListedColormap(color_palette))
        ax1.axhline(x, linewidth=2, color='k')
        ax1.axvline(y, linewidth=2, color='k')
        ax1.set_title(emo)
        ax1.set_xticks([]); ax1.set_yticks([])
        sns.despine(bottom=True, left=True, ax=ax1)

        this_clean_df = full_data_df.loc[:, [(x, y)] ]
        ax2 = plt.subplot(2,7,n+7+1)
        
        sns.barplot(data=this_clean_df, alpha=0.8, ax=ax2, color=palette7[n], zorder=0)
        sns.stripplot(data=this_clean_df, size=5, edgecolor='white', linewidth=1, alpha=0.4, color=palette7[n], clip_on=False, zorder=1, ax=ax2)
        sns.barplot(data=this_clean_df, alpha=0, ax=ax2, color=palette7[n], ci=settings_dict['ci'], zorder=99)

        sns.despine(bottom=True, ax=ax2)
        ax2.set_ylim(-1, 1)
        ax2.axhline(0, color='k')
        if n > 0:
            sns.despine(left=True, bottom=True, ax=ax2)
            ax2.set_yticks([])
        
        mean = this_clean_df.mean()[0]
        ax2.set_xticks([])
        ax2.set_xlabel(f'{mean:.2f}')
        
    plt.close()
    
    return fig


# In[31]:


my_plot(100, 100, 'clean')


# #### Specify ranges

# In[32]:


x_params = (60, 160, 20, 80)
y_params = (30, 120, 20, 70)


# In[33]:


x_range = range(x_params[0], x_params[1], x_params[2])
y_range = range(y_params[0], y_params[1], y_params[2])


# How many widget states necessary?

# In[34]:


my_max_opts = max(len(x_range), len(y_range))
my_max_opts


# How many states overall?

# In[35]:


my_max_states = len(x_range)*len(y_range)*2
my_max_states


# ### make Dashboard

# In[36]:


dashboard = pn.interact(my_plot, comparison=['baseline', 'clean'], x=x_params, y=y_params)


# ##### with running kernel

# In[ ]:


dashboard


# ##### example screenshot

# In[38]:


dashboard.save('../reports/figures/screenshot_interact.png', as_png=True)


# ##### all possible stored in html

# In[ ]:


dashboard.embed(max_opts=my_max_opts, max_states=my_max_states)


# In[ ]:


dashboard.save('../reports/figures/face_panel.html',
               embed=True,
               max_opts=my_max_opts,
               max_states=my_max_states)

