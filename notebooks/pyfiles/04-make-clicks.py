#!/usr/bin/env python
# coding: utf-8

# # Analyse Click Counts

# ### import modules

# In[1]:


import json
import pickle

import numpy as np
import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import seaborn as sns
sns.set_context('talk')

from matplotlib import animation
from IPython.display import HTML

from matplotlib.offsetbox import OffsetImage, AnnotationBbox


# ### load settings

# In[2]:


with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)

with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)
    
with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# ### get data

# In[3]:


data_df = pd.read_csv('../data/interim/big_df.tsv', sep='\t', index_col=[0, 1], header=[0, 1, 2])


# convert number strings to integers

# ### get clicks

# In[4]:


clean_df = data_df.drop(['coords'], axis=0, level=1)
clean_df = pd.concat([clean_df.drop(['Answer'], level=1).applymap(lambda x:eval(x)),
                      clean_df.drop(['upper', 'lower', 'diff'], level=1)
                     ])
clean_df = clean_df.sort_index()
clean_df.tail()


# ### Explore Click Numbers
# 
# Because Total Number of Clicks was limited to 10, regardless of condition, it could be that in the Full condition participants were not able to click on all parts they wanted. This should be reflected in celing effects for the Sums of Lower and Upper Condition.
# 
# E.g., the Sum of Clicks for the Lower and Upper Mask Condition should be higher than for the Full Condition.

# In[5]:


palette3 = [palette7[0], palette7[0], palette7[3], palette7[-1], palette7[2]]

for half in ['upper', 'lower']:
    fig = plt.figure(figsize=(16, 4))
    for n, cond in enumerate(['Full','FullSum', 'Lower', 'Upper', 'HalfSum']):
        ax = plt.subplot(1, 5, n+1)
        
        if cond == 'FullSum':
            ns = clean_df.loc[:, 'Full'].xs('lower', axis=0, level=1) + clean_df.loc[:, 'Full'].xs('upper', axis=0, level=1)
            ns = ns.values.flatten()
            
        elif cond == 'HalfSum':
            ns = clean_df.loc[:, 'Lower'].xs('lower', axis=0, level=1) + clean_df.loc[:, 'Upper'].xs('upper', axis=0, level=1)
            ns = ns.values.flatten()
        else:
            ns = clean_df.loc[:, cond].xs(half, axis=0, level=1).values.flatten()
        
        sns.histplot(ns, bins=11, binrange=(0, 10), color=palette3[n], ax=ax)
        ax.set_title(cond)
        ax.set_ylim(0, 1500)
    
    sns.despine()
    plt.tight_layout()
    plt.suptitle(half)
    plt.show()


# In[6]:


this_palette = [palette7[0], palette7[-1]]

fig = plt.figure(figsize=(8, 4))
ax = plt.subplot(1, 1, 1)
for n, cond in enumerate(['FullSum', 'HalfSum']):
    
    if cond == 'FullSum':
        ns = clean_df.loc[:, 'Full'].xs('lower', axis=0, level=1) + clean_df.loc[:, 'Full'].xs('upper', axis=0, level=1)
        ns = ns.values.flatten()
 
    elif cond == 'HalfSum':
        ns = clean_df.loc[:, 'Lower'].xs('lower', axis=0, level=1) + clean_df.loc[:, 'Upper'].xs('upper', axis=0, level=1)
        ns = ns.values.flatten()
    
    else:
        pass
    
    sns.histplot(ns, bins=11, binrange=(0, 10), color=this_palette[n], ax=ax, label=cond)
    
ax.set_ylim(0, 1500)
plt.legend()
sns.despine(trim=True)
plt.tight_layout()
plt.show()


# ## Visualization

# ### transform data for plotting

# #### get only correct answers

# In[7]:


correct_df = clean_df.T.stack(0)
correct_df = correct_df[correct_df.index.get_level_values(1) == correct_df.loc[:, 'Answer'].values]
correct_df.tail()


# #### average over the 10 face identities, as proportion

# In[8]:


correct_df = correct_df.drop('Answer', axis=1).groupby(level=[0, 1, 3]).mean()
correct_df = correct_df / settings_dict['n_answers']

correct_df.tail()


# #### only full condition and only differences

# In[9]:


full_diff_df = pd.DataFrame(correct_df.loc['Full', 'diff']).unstack(0)['diff']
full_diff_df.tail()


# store for later re-use

# In[10]:


full_diff_df.to_csv('../data/processed/click_diff_correct_df.tsv', sep='\t')


# #### make long format

# In[11]:


full_long_df = pd.melt(full_diff_df.reset_index(), id_vars='index', value_name='diff', var_name='emotion' )
full_long_df.tail()


# ## Make Big Plot

# In[12]:


big_abs_df = correct_df.copy()


# In[13]:


big_abs_df.loc[:, 'full'] = big_abs_df.loc[:, 'lower'] + big_abs_df.loc[:, 'upper']
big_abs_df = big_abs_df.T['Full'].stack(1).drop('diff', level=0)
big_abs_df = big_abs_df.unstack(0).reorder_levels([1, 0], axis=1).sort_index(axis=1)


# In[14]:


big_abs_df = big_abs_df.rename({'full': 'Full', 'lower' : 'Lower', 'upper': 'Upper'}, axis=1, level=0)


# In[15]:


big_abs_df


# In[16]:


long_abs_df = pd.melt(big_abs_df.reset_index(), 
                          id_vars='index', 
                          var_name=['cond', 'emotion'], 
                          value_name='% clicks')
long_abs_df


# In[17]:


with open('../models/primary_palette.pkl', 'rb') as fp:
    primary_palette = pickle.load(fp)
    
palette3 = primary_palette[:3]
sns.palplot(palette3)


# In[18]:


fig, ax = plt.subplots(1,1, figsize=(16, 6))

sns.barplot(data=long_abs_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=0,
            ax=ax)

# add jitter to plot swarms
jitter_df = long_abs_df.copy()

sns.swarmplot(data=jitter_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=2,
            alpha=0.8,
            dodge=True,
            zorder=2,
            ax=ax)

sns.barplot(data=long_abs_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=my_boot, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

nconds = len(set(long_abs_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)

plt.ylim(-0.2, 1) # make room for annotations below figure
plt.xticks(y=-0)

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.15)
    ab = AnnotationBbox(imagebox, (x, y-0.05), pad=0, zorder=1)
    ax.add_artist(ab)
    
ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=22)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
plt.savefig('../reports/figures/click_number_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ### version 2, with different masking

# In[19]:


big_abs_df = correct_df.copy()
big_abs_df.loc[:, 'full'] = big_abs_df.loc[:, 'lower'] + big_abs_df.loc[:, 'upper']


# In[20]:


big_abs_df = big_abs_df.loc[:, ['full']]


# In[21]:


big_abs_df = big_abs_df.unstack([0, 1])['full']


# In[22]:


big_abs_df = big_abs_df.rename({'full': 'Full', 'lower' : 'Lower', 'upper': 'Upper'}, axis=1, level=0)


# In[23]:


long_abs_df = pd.melt(big_abs_df.reset_index(), 
                          id_vars='index', 
                          var_name=['cond', 'emotion'], 
                          value_name='% clicks')
long_abs_df


# In[24]:


fig, ax = plt.subplots(1,1, figsize=(16, 6))

sns.barplot(data=long_abs_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=0,
            ax=ax)

# add jitter to plot swarms
jitter_df = long_abs_df.copy()

sns.swarmplot(data=jitter_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=2,
            alpha=0.8,
            dodge=True,
            zorder=2,
            ax=ax)

sns.barplot(data=long_abs_df, 
            x='emotion', 
            y='% clicks', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=my_boot, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

nconds = len(set(long_abs_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)

plt.ylim(-0.2, 1) # make room for annotations below figure
plt.xticks(y=-0)

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.15)
    ab = AnnotationBbox(imagebox, (x, y-0.05), pad=0, zorder=1)
    ax.add_artist(ab)
    
ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=22)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
#plt.savefig('../reports/figures/click_number_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Make Difference Plot

# In[25]:


fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi=100)

sns.barplot(data=full_long_df, 
            x='emotion', 
            y='diff', 
            order=my_order, 
            ci=my_ci, 
            palette=palette7,
            zorder=0,
            errwidth=0,
            ax=ax)

sns.swarmplot(data=full_long_df, 
              x='emotion', 
              y='diff', 
              order=my_order, 
              palette=palette7,
              linewidth=0.5,
              edgecolor='white',
              size=2,
              alpha=0.8,
              zorder=2,
              ax=ax)

sns.barplot(data=full_long_df, 
            x='emotion', 
            y='diff', 
            order=my_order, 
            ci=my_ci, 
            palette=palette7,
            errwidth=5,
            n_boot=my_boot,
            alpha=0,
            zorder=2,
            ax=ax)

# zero line
plt.axhline(0, c='k', linestyle=':')

# neutral line
mean_ntr = full_long_df[full_long_df['emotion']=='Neutral']['diff'].mean()
this_color = palette7[np.where(np.array(my_order)=='Neutral')[0][0]]
plt.axhline(mean_ntr, linestyle=':', color=this_color)

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.1)
    ab = AnnotationBbox(imagebox, (x, -.4), pad=0, zorder=1)
    ax.add_artist(ab)

# abbreviated ticks
ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()])
plt.xticks(y=0.03)
#plt.ylim(-.4, .4, .1)

plt.ylabel('Upper-Lower')

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
ax.tick_params(bottom=False)
sns.despine(bottom=True, trim=True)

plt.savefig('../reports/figures/click_diff_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ### make animation plot

# In[26]:


from matplotlib import animation
from IPython.display import HTML


# In[27]:


correct_df


# In[28]:


big_diff_df = correct_df.loc['Full', ['diff']].unstack(0)['diff']
big_diff_df.tail()


# In[29]:


p_nums = big_diff_df.index


# In[30]:


fig, ax = plt.subplots(1, 1, figsize=(16, 6))

def animate(i):
    
    ax.clear()
    ax.set_title(f'sample size = {i}') 

    this_big_df = big_diff_df.drop(p_nums[i:]) 
    this_df = pd.melt(this_big_df.reset_index(), 
                           id_vars='index', 
                           var_name='emotion', 
                           value_name='Upper-Lower')

    my_order = this_big_df.mean().sort_values().index

    sns.barplot(data=this_df, 
                x='emotion', 
                y='Upper-Lower', 
                order=my_order, 
                palette=palette7,
                errwidth=5,
                n_boot=my_boot,
                ci=my_ci,
                ax=ax)

    jitter_df = this_df.copy()
    jitter_df.loc[:, ['Upper-Lower']] = jitter_df.loc[:, ['Upper-Lower']].applymap(lambda x:x + (np.random.rand()-0.5)*0.03)

    sns.swarmplot(data=jitter_df, 
                  x='emotion', 
                  y='Upper-Lower', 
                  order=my_order, 
                  palette=palette7,
                  linewidth=0.5,
                  edgecolor='grey',
                  size=3,
                  alpha=0.5,
                  zorder=99,
                  ax=ax)

    ax.axhline(0, c='k', linestyle=':')
    sns.despine(bottom=True, ax=ax)
    plt.legend([])
    
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))
    
    ax.set_ylim(-.4, .6)

ani = animation.FuncAnimation(fig, animate, frames=range(1, len(p_nums)+1), repeat=False);


# In[31]:


my_html = ani.to_jshtml()


# In[32]:


HTML(my_html)

