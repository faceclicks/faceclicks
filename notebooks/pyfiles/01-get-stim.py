#!/usr/bin/env python
# coding: utf-8

# # Get Stimuli

# ### import modules

# In[1]:


import re
import glob
import json
import pickle

import numpy as np
import pandas as pd
import cv2

from PIL import Image, ImageDraw
from matplotlib import image

import matplotlib.pyplot as plt
import seaborn as sns

sns.set_context("talk")

from tqdm.notebook import tqdm


# ### get data

# In[2]:


raw_df = pd.read_csv(
    "../data/raw/raw_data_publish.tsv",
    sep="\t",
    low_memory=False,
    index_col=[0],
    header=[0],
)
raw_df.tail()


# ### get image infos from logfile
# 
# Get the names from the columns of the data table

# In[3]:


face_names = set([c.split("_")[0] for c in raw_df.columns if ( c.startswith("AF") or c.startswith("AM") ) and c.endswith("Auswahl")])
face_names = sorted(face_names)


# In[4]:


face_names[::3]


# ### get image infos from filenames
# 
# The KDEF images (original and masked) need to be in the ```stim``` folder. These data are not provided in this repository for copyright reasons. 

# In[5]:


ims = glob.glob("../stim/*.jpg")
ims = sorted(ims)


# In[6]:


[i[8:-4] for i in ims[::3]]


# In[7]:


regex = re.compile(".*(A\w+\d+\w+)[-]?(\w+)?\.\w+")


# In[8]:


idx = pd.MultiIndex.from_tuples([regex.findall(i)[-1] for i in ims])


# In[9]:


stim_df = pd.DataFrame(ims, index=idx)
stim_df.columns = ["file_name"]


# In[10]:


stim_df


# In[11]:


in_stim_dict = {}
for face_name in face_names:

    try:
        name, cond = face_name.split(" ")
    except:
        name, cond = face_name, ""

    in_stim = False
    for (stim_name, stim_cond) in stim_df.index:
        if (name, cond) == (stim_name, stim_cond):
            in_stim = True

    in_stim_dict[face_name] = in_stim


# In[12]:


pd.DataFrame(in_stim_dict, index=["status"]).T.sort_values(by="status")


# In[13]:


assert (stim_df.groupby(level=[0, 1]).first().shape == stim_df.shape), "warning, contains duplicates"


# In[14]:


assert (    len(face_names) == stim_df.shape[0]), "warning, logfile-stim and file-stim numbers do not match"


# #### dictionary for renaming

# In[15]:


with open("../references/cond_dict.json", "r") as fp:
    cond_dict = json.load(fp)


# In[16]:


stim_df = stim_df.rename(cond_dict)
stim_df.tail()


# ### add cut infos
# 
# The faces in the images are well-aligned (see average image later), but there are still slight differences. Here, we define by hand a horizontal line slightly above the tip of the nose of each face. Subsequently, this can be visually validated.

# In[17]:


for i in stim_df.index:
    if i[1] == "Lower":

        im = stim_df.loc[i, "file_name"]

        a = plt.imread(im)
        b = a == [255, 255, 255]

        for p in range(b.shape[0]):
            this = b[p][300]
            if not this.all():
                for j in stim_df.index.levels[1]:
                    stim_df.loc[(i[0], j), "cut"] = p
                break


# In[18]:


stim_df


# #### dictionary for renaming

# In[19]:


cat_dict = {
    "AFS": "Fearful",
    "ANS": "Angry",
    "DIS": "Disgusted",
    "HAS": "Happy",
    "NES": "Neutral",
    "SAS": "Sad",
    "SUS": "Surprised",
}

with open("../references/cat_dict.json", "w") as fp:
    json.dump(cat_dict, fp)

with open("../references/cat_dict.json", "r") as fp:
    cat_dict = json.load(fp)

cat_dict


# In[20]:


regex = re.compile(r"(\w+\d+)(\w+)")
idx = [
    (regex.findall(i)[-1][0], regex.findall(i)[-1][1], j) for (i, j) in stim_df.index
]
stim_df.index = pd.MultiIndex.from_tuples(idx)
stim_df = stim_df.rename(cat_dict)


# In[21]:


stim_df


# #### reorder

# In[22]:


stim_df = stim_df.reorder_levels([2, 1, 0]).sort_index()
stim_df


# store

# In[23]:


stim_df.to_csv("../data/interim/stim_df.tsv", sep="\t", index=[0, 1, 2])


# re-load

# In[24]:


stim_df = pd.read_csv("../data/interim/stim_df.tsv", sep="\t", index_col=[0, 1, 2])
stim_df


# In[25]:


stim_df.groupby(level=[1, 2]).mean().loc[:, "cut"].std() / plt.imread(ims[0]).shape[0] * 100


# ## visualize

# #### show an image

# In[26]:


def load_im(file_name):
    
    img = cv2.imread(file_name,  cv2.IMREAD_GRAYSCALE)
    
    return img


# In[27]:


file_name = stim_df.loc[("Full", "Surprised", "AF30"), "file_name"]
file_name


# In[ ]:


res = load_im(file_name)
plt.imshow(res, cmap="Greys_r")
plt.show()


# In[29]:


file_name = stim_df.loc[("Upper", "Surprised", "AF30"), "file_name"]
file_name


# In[ ]:


res = load_im(file_name)
plt.imshow(res, cmap='Greys_r')
plt.show()


# #### show upper/lower cutoff

# In[ ]:


for cond in stim_df.index.levels[0]:

    for emo in stim_df.index.levels[1]:

        fig = plt.figure(figsize=(16, 3))

        this_emo = stim_df.loc[(cond, emo), :]

        for n, i in enumerate(this_emo.index):
            ax = plt.subplot(1, this_emo.shape[0], n + 1)

            this_im = this_emo.loc[i, "file_name"]
            this_cut = this_emo.loc[i, "cut"]

            res = load_im(this_im)
            ax.imshow(res, cmap="Greys_r")
            ax.set_title(i)
            ax.set_yticks([])
            ax.set_xticks([])
        plt.suptitle(emo)
        plt.tight_layout()
        plt.show()


# ## Make average color images

# Use full condition

# In[32]:


full_df = stim_df.loc['Full']


# get dimensions of one example image as template

# In[33]:


example_im = Image.open( full_df.iloc[0,0])
dims = (example_im.width, example_im.height)


# ### average for every condition separately

# In[34]:


my_crop = (100, 150, 470, 650)


# In[35]:


d = {}

# for each emotion
for n, i in enumerate(full_df.index.levels[0]):
    
    # make blank image first
    mean_im = Image.new('RGB', dims)
    
    # get all images
    ims = full_df.loc[i, 'file_name'].values

    # add each image to blend
    for m, im in enumerate(ims):
        # get the image
        im = Image.open(im)
        
        # every time a new image is added, it is weighted less (first one is 1, last one is 1/10 (out of 10))
        this_alpha = 1/(m+1)
        mean_im = Image.blend(mean_im, im, alpha=this_alpha)
        
        # store in dict
        d[i] = mean_im.crop(my_crop)


# #### visualize averaged images

# In[36]:


fig = plt.figure(figsize=(16, 6))
for n, i in enumerate(d):
    
    ax = plt.subplot(1, len(d.keys()), n+1)
    
    ax.imshow(d[i])
    ax.set_title(i)
    ax.set_xticks([]); ax.set_yticks([])
    
plt.show()


# #### store for later re-use

# In[37]:


with open('../models/average_im_dict.pkl', 'wb') as fp:
    pickle.dump(d, fp)

