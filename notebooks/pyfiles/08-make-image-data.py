#!/usr/bin/env python
# coding: utf-8

# # Get Click Coordinates

# ### import packages

# In[1]:


import pickle
import json

import numpy as np
import pandas as pd

import cv2
from skimage import filters
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from matplotlib.colors import ListedColormap

from tqdm.notebook import tqdm


# ### get color palettes

# In[2]:


with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/red_palette.pkl', 'rb') as fp:
    red_palette = pickle.load(fp)


# ### get stimuli

# In[3]:


stim_df = pd.read_csv('../data/interim/stim_df.tsv', sep='\t', index_col=[0, 1, 2])
stim_df.tail()


# In[4]:


def load_im(file_name):
    
    img = cv2.imread(file_name,  cv2.IMREAD_GRAYSCALE)
    x_shape, y_shape = img.shape
    scale = 1 # use original dimensions
    new_shape = (int(y_shape*scale), int(x_shape*scale))
    res = cv2.resize(img, dsize=new_shape)
    
    return res


# Example condition

# In[5]:


im_key = ('Full', 'Fearful', 'AF01')


# In[ ]:


file_name = stim_df.loc[im_key, 'file_name']
res = load_im(file_name)
plt.imshow(res, cmap='Greys_r')
plt.show()


# get prototypical image shape 

# In[7]:


n_rows, n_cols = res.shape
n_rows, n_cols


# ### get data

# In[8]:


data_df = pd.read_csv('../data/interim/big_df.tsv', sep='\t', index_col=[0, 1], header=[0, 1, 2])
data_df.head(5)


# ### put coordinates into numpy array

# In[9]:


def make_array(p_num, im_key, data_df, n_rows=n_rows, n_cols=n_cols):
    
    these_coords = eval(data_df.loc[(p_num, 'coords'), im_key])

    zero_array = np.zeros((n_rows, n_cols))
    
    for (y, x) in these_coords:
        x, y = int(x), int(y)
        zero_array[(x, y)] = 1
    
    return zero_array


# #### example participant

# In[10]:


p_num = data_df.index.levels[0][0]
p_num


# In[11]:


zero_array = make_array(p_num, im_key, data_df)


# In[12]:


plt.imshow(zero_array, vmin=0, vmax=0.00000001)


# ### increase size of clicks

# In[13]:


def make_smooth(zero_array, my_sigma=10, my_binarizer=.0001):  # OR: .0005 ?
    
    fa = filters.gaussian(zero_array, sigma=my_sigma)
    fa = fa>my_binarizer
    fa = fa.astype(int)
    
    return fa


# In[14]:


bin_array = make_smooth(zero_array)


# In[ ]:


plt.figure(figsize=(16, 24))
plt.imshow(res, cmap='Greys_r')
plt.imshow(pd.DataFrame(bin_array).replace(0, np.nan).values);


# ### get all identities of one emo/condition for one participant

# In[16]:


def make_big_zero_dict(p_num, emo, cond, data_df, n_rows=n_rows, n_cols=n_cols):
    
    big_zero_dict = {}

    for ident in data_df.columns.levels[2]:
        
        im_key = (cond, emo, ident)
        zero_array = make_array(p_num, im_key, data_df)
        bin_array = make_smooth(zero_array)

        answer = data_df.loc[(p_num, 'Answer'), im_key]
        
        big_zero_dict[(answer,ident)] = bin_array

    return big_zero_dict


# In[17]:


big_zero_dict = make_big_zero_dict(p_num, 'Fearful', 'Full', data_df)


# In[18]:


def make_big_zero_array(big_zero_dict, ans):
    
    big_zero_array = np.zeros_like( big_zero_dict[ list(big_zero_dict.keys())[-1] ] )
    
    for i in big_zero_dict:
        if i[0] == ans:
            big_zero_array += big_zero_dict[i]

    return big_zero_array


# In[19]:


big_zero_array = make_big_zero_array(big_zero_dict, 'Fearful')


# In[20]:


plt.figure(figsize=(4,4))

my_min = 0
my_max = big_zero_array.max()

plt.imshow(res, cmap='Greys_r')
plt.imshow(big_zero_array,
           vmin=my_min, vmax=my_max, cmap='Reds');

plt.colorbar()
plt.show()


# In[21]:


pos_palette = color_palette[int(len(color_palette)/2):]


# In[ ]:


plt.figure(figsize=(4,4))

my_min = 0
my_max = big_zero_array.max()

plt.imshow(res, cmap='Greys_r')
plt.imshow(pd.DataFrame(big_zero_array).replace(0, np.nan).values,
           vmin=my_min, vmax=my_max, cmap=ListedColormap(pos_palette))

plt.colorbar()
plt.show()


# ### make methods plot

# In[23]:


biggest_im = np.zeros((n_rows, n_cols))
this_stim_df = stim_df.loc['Full']
for i in this_stim_df['file_name']:
    im_name = load_im(i)
    biggest_im += im_name


# In[24]:


fig = plt.figure(figsize=(5, 9), dpi=100)
plt.imshow(biggest_im, cmap='Greys_r')
plt.show()


# In[25]:


fig = plt.figure(figsize=(20, 8), dpi=300)

ax1 = plt.subplot(1, 3, 1)
ax1.imshow(biggest_im, cmap='Greys_r')

y, x = np.where(zero_array)
for i in range(len(x)):
    ax1.scatter(x[i], y[i], marker='+', color='white', s=600, linewidths=5)

ax2 = plt.subplot(1, 3, 2)
ax2.imshow(biggest_im, cmap='Greys_r')
ax2.imshow(pd.DataFrame(bin_array).replace(0, np.nan).values, cmap=ListedColormap(red_palette[::-1]))

ax3 = plt.subplot(1, 3, 3)
ax3.imshow(biggest_im, cmap='Greys_r')
imax3 = ax3.imshow(pd.DataFrame(big_zero_array).replace(0, np.nan).values/10,
           vmin=my_min, vmax=1, cmap=ListedColormap(pos_palette));

ax1.set_axis_off()
ax2.set_axis_off()
ax3.set_axis_off()

fs = 36
ax1.set_title('Coordinates', fontsize=fs)
ax2.set_title('Circles', fontsize=fs)
ax3.set_title('Average', fontsize=fs)

fig.colorbar(imax3, shrink=0.9)

plt.tight_layout()

plt.savefig('../reports/figures/methods_clicks.png', dpi=300, bbox_inches='tight')
plt.show()


# ### df, by face ident

# In[26]:


def make_this_flat(emo, cond, p_num, data_df):
    
    big_zero_dict = make_big_zero_dict(p_num, emo, cond, data_df)
    
    flat_dict = {}
    for ans, i in big_zero_dict:
        flat_dict[(ans, i)] = big_zero_dict[(ans, i)].flatten()
    flat_df = pd.DataFrame(flat_dict).T
    
    flat_df.index = pd.MultiIndex.from_tuples([(cond, emo, i[0], i[1], p_num) for i in flat_df.index])
    flat_df = flat_df.sort_index()
    
    return flat_df


# In[27]:


flat_df = make_this_flat('Happy', 'Full', p_num, data_df)


# In[28]:


flat_df


# ## Reduce Image Size to deal with memory issues

# ### scaling factor
# 
# This allows to downsample the data to aviod memory problems

# In[29]:


scale_factor = 4


# ### make average image
# 
# For visualization purposes

# In[30]:


biggest_im = np.zeros((n_rows, n_cols))[::scale_factor, ::scale_factor]
this_stim_df = stim_df.loc['Full']
for i in this_stim_df['file_name']:
    im_name = load_im(i)[::scale_factor, ::scale_factor]
    biggest_im += im_name


# In[31]:


np.savetxt('../data/interim/biggest_im.txt', biggest_im)


# ### define how to bin the data

# In[32]:


def make_scale_bins(scale_factor, n_rows=n_rows, n_cols=n_cols):    
    
    sf = scale_factor
    
    x_bins = np.array([[i]*sf for i in range(round(n_rows/sf)+sf)]).flatten()[:n_rows]
    y_bins = np.array([[i]*sf for i in range(round(n_cols/sf)+sf)]).flatten()[:n_cols]
    
    return x_bins, y_bins


# In[33]:


x_bins, y_bins = make_scale_bins(scale_factor)


# In[34]:


x_bins.shape, y_bins.shape


# Dimensions of Downscaled Data

# In[35]:


new_n_rows, new_n_cols = np.unique(x_bins).shape[0], np.unique(y_bins).shape[0]
new_n_rows, new_n_cols


# #### store for later re-use

# In[36]:


dim_dict = {'n_rows': n_rows,
            'n_cols': n_cols,
            'new_n_rows': new_n_rows,
            'new_n_cols': new_n_cols,
            'scale_factor':scale_factor
           }


# In[37]:


with open('../data/interim/im_dims.json', 'w') as fp:
    json.dump(dim_dict, fp)


# ### apply binning to already flattened data

# In[38]:


def make_downsample(this_data, n_rows=n_rows, n_cols=n_cols, x_bins=x_bins, y_bins=y_bins):
    
    # bring back to original shape
    im = this_data.values.reshape(n_rows, n_cols)
    # make dataframe                
    new_im = pd.DataFrame(im)
    # add binning to multiindex
    new_im.index = pd.MultiIndex.from_arrays([x_bins, new_im.index])
    new_im.columns = pd.MultiIndex.from_arrays([y_bins, new_im.columns])
    # average over bins
    lo_im = new_im.groupby(level=0, axis=0).max().groupby(level=0, axis=1).max()
    # make flat again
    new_data = lo_im.values.flatten()

    return new_data


# In[39]:


my_example = flat_df.iloc[0, :]
make_downsample(my_example).shape


# ### for all in one package

# In[40]:


def make_all_downsample(flat_df):

    new_df = pd.DataFrame()
    for i in flat_df.index:
        
        this_data  = flat_df.loc[i]
        
        down_data = make_downsample(this_data)
        
        this_df = pd.DataFrame(down_data).T
        this_df.index = pd.MultiIndex.from_tuples([i])
        
        new_df = pd.concat([new_df, this_df])
        
    return new_df


# In[41]:


make_all_downsample(flat_df)


# ### collect all data
# 
# (Downsampled to avoid memory issues)

# In[ ]:


def make_big_flat_df(cond, data_df):
    
    p_nums = data_df.index.levels[0]
    emos = data_df.columns.levels[1]
    
    big_flat_df = pd.DataFrame()
    
    for emo in tqdm(emos, leave=False):
        
        emo_df = pd.DataFrame()
    
        for p_num in tqdm(p_nums, leave=False): 
            
            flat_df = make_this_flat(emo, cond, p_num, data_df)
            new_df = make_all_downsample(flat_df) # downsample
            
            emo_df = pd.concat([emo_df, new_df])

        big_flat_df = pd.concat([big_flat_df, emo_df])

    big_flat_df = big_flat_df.sort_index()
    
    return big_flat_df


# #### do for all conditions and store by condition

# This takes a long time!

# In[ ]:


for cond in tqdm(stim_df.index.levels[0]):
    biggest_flat_df = make_big_flat_df(cond, data_df)
    
    with open(f'../data/interim/biggest_{cond}_flat_df.pkl', 'wb') as fp:
        pickle.dump(biggest_flat_df.loc[cond, :], fp)


# ## Store, averaged by Images
# 
# This is better for comparing masking conditions, because other wise the 3 masking condition need to have the same answers for the same image-identity in order to give non-nan values

# In[42]:


for cond in tqdm(stim_df.index.levels[0]):
        
    with open(f'../data/interim/biggest_{cond}_flat_df.pkl', 'rb') as fp:
        this_biggest_flat_df = pickle.load(fp)
    
    # average over the 10 image identities
    this_big_flat_df = this_biggest_flat_df.groupby(level=[0, 1, 3]).mean()
    
    # make global average for each participant
    aa = this_biggest_flat_df.groupby(level=3).mean()
    aa.index = pd.MultiIndex.from_tuples([('Mean', 'Mean', i) for i in aa.index])
    
    # add each participant's global average
    this_big_flat_df = pd.concat([this_big_flat_df, aa])
    this_big_flat_df = this_big_flat_df.sort_index()
    
    # store
    this_big_flat_df.to_csv(f'../data/processed/big_{cond}_baseline_df.tsv', sep='\t')
    
    # count number of non-zero answers and store
    this_n_flat_df = (this_big_flat_df>0).groupby(level=[0, 1]).sum()
    this_n_flat_df.to_csv(f'../data/processed/n_{cond}_baseline_df.tsv', sep='\t')


# ## Global Mean removed

# ### get number participants

# In[43]:


def make_big_clean_df(clean_df):

    big_df = pd.DataFrame()
    n_df = pd.DataFrame()

    # global means for each participant
    other_df = clean_df.loc[('Mean', 'Mean'), :]
        
    for emo1 in clean_df.index.levels[0].drop('Mean'):

        emo_df = clean_df.loc[emo1, :]
        emo_df.index = emo_df.index.remove_unused_levels()
        
        for emo2 in emo_df.index.levels[0]:

            this_df = clean_df.loc[(emo1, emo2), :]    

            diff_df = this_df - other_df
            diff_df = diff_df.dropna()

            diff_df.index = pd.MultiIndex.from_tuples([(emo1, emo2, i) for i in diff_df.index])

            thresh_df = (this_df * other_df) > 0
            thresh_df.index = pd.MultiIndex.from_tuples([(emo1, emo2, i) for i in thresh_df.index])
            # sum of participants
            this_n_df = thresh_df.groupby(level=[0, 1]).sum()

            big_df = pd.concat([big_df, diff_df])
            n_df = pd.concat([n_df, this_n_df])
            
    return big_df, n_df


# In[44]:


for cond in tqdm(stim_df.index.levels[0]):
        
    this_big_flat_df = pd.read_csv(f'../data/processed/big_{cond}_baseline_df.tsv', sep='\t', index_col=[0, 1, 2])
    big_df, n_df = make_big_clean_df(this_big_flat_df)
    
    big_df.to_csv(f'../data/processed/big_{cond}_clean_df.tsv', sep='\t')
    n_df.to_csv(f'../data/processed/n_{cond}_clean_df.tsv', sep='\t')


# ## Make Difference between Full and Halves

# In[45]:


def make_sum_df(this_df, other_df):
    
    # because differences can sum up to zero, we need to multiply to make
    # sure that the participant has a non-zero value in both conditions
    thresh_df = (this_df * other_df) > 0
    # sum of participants
    this_n_df = thresh_df.groupby(level=[0, 1]).sum()
    
    return this_n_df 


# In[46]:


for comparison in tqdm(['baseline', 'clean']):

    full_df = pd.read_csv(f'../data/processed/big_Full_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])

    for cond in tqdm(['Upper', 'Lower'], leave=False):

        # masking condition
        mask_df = pd.read_csv(f'../data/processed/big_{cond}_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])

        # difference of full and masked
        diff_df = (full_df - mask_df).dropna()
        diff_df.to_csv(f'../data/processed/big_Full_gr_{cond}_{comparison}_df.tsv', sep='\t')

        # count of participants 
        this_n_df = make_sum_df(full_df, mask_df)
        this_n_df.to_csv(f'../data/processed/n_Full_gr_{cond}_{comparison}_df.tsv', sep='\t')


# ## Make difference of correct vs. confusions

# In[47]:


full_df = pd.read_csv(f'../data/processed/big_Full_baseline_df.tsv', sep='\t', index_col=[0, 1, 2])


# In[48]:


diff_df = pd.DataFrame()
n_df = pd.DataFrame()

emos = full_df.index.levels[0].drop('Mean')

for cond in tqdm(emos):

    correct_df = full_df.loc[(cond, cond)]
    
    for other in tqdm(emos, leave=False):

        try:
            other_df = full_df.loc[(cond, other)]
            this_diff_df = (other_df - correct_df).dropna()
            this_diff_df.index = pd.MultiIndex.from_tuples([(cond, other, i) for i in this_diff_df.index])
            
            # count of participants 
            thresh_df = (correct_df * other_df) > 0
            this_n_df = thresh_df.sum()
            this_n_df = pd.DataFrame(this_n_df).T
            this_n_df.index = pd.MultiIndex.from_tuples([(cond, other)])

            diff_df = pd.concat([diff_df, this_diff_df])
            n_df = pd.concat([n_df, this_n_df])
        except:
            pass
            
diff_df = diff_df.sort_index()
n_df = n_df.sort_index()


# In[49]:


diff_df.to_csv(f'../data/processed/big_Full_confusions_df.tsv', sep='\t')
n_df.to_csv(f'../data/processed/n_Full_confusions_df.tsv', sep='\t')

