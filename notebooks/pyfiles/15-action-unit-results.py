#!/usr/bin/env python
# coding: utf-8

# # Compute AU statistics

# ### import packages

# In[1]:


import json
import pickle

import itertools
import re

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from matplotlib.lines import Line2D


# ### AU infos
# 
# from http://www.scholarpedia.org/article/Facial_expression_analysis
# 
# | Emotion   | AUs                           |
# |-----------|-------------------------------|
# | Anger     |  4, 5 and/or 7,  22, 23, 24   |
# | Disgust   |    9 and/or 10,  (25 or 26)   |
# | Fear      | 1, 2, 4, 5, 7, 20, (25 or 26) |
# | Happiness | 6, 12                         |
# | Sadness   | 1, (4), 15, (17)              |
# | Surprise  |1, 2, 5, 25 o 26               |
# | Fear      | 1, 2, 4, 5, 7, 20, (25 or 26) | 	

# ### settings 

# In[2]:


with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)

with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)

with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)

with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# ### get data

# In[3]:


df = pd.read_csv('../data/processed/big_p_au_df.tsv',
            sep='\t',
            index_col=[0, 1, 2],
            header=[0, 1])


# In[4]:


df


# ### clean up

# Image is not interesting here

# In[5]:


df = df.drop([('Image', 'Image')], axis=1)


# ### average over participants

# In[6]:


mean_df = df.groupby(level=[1, 2]).mean()


# In[7]:


mean_df.tail()


# ### only full condition

# In[8]:


full_df = mean_df.loc['Full', :]


# In[9]:


full_df


# ### correlation

# In[10]:


sns.heatmap(full_df.T.corr(), 
            cmap=color_palette, 
            vmin=-1, vmax=1, 
            square=True, 
            annot=True, 
            fmt='.1f')


# In[11]:


sns.clustermap(full_df.T.corr(), 
               cmap=color_palette, 
               vmin=-1, 
               vmax=1, 
               square=True, 
               annot=True, 
               fmt='.1f')


# In[12]:


full_df.index


# In[13]:


plt.figure(figsize=(16, 6))
sns.heatmap(full_df.loc[my_order, :],
            cmap=color_palette,
            square=True)
for i in [4,7,9,11,14]:
    plt.axvline(i, linewidth=5, color='white')
for i in range(7):
    plt.axhline(i, linewidth=5, color='white')


# ### get full condition

# In[14]:


full_df = df.xs('Full', level=1)
full_df.tail()


# get mean for the AU over all conditions for that participant

# In[15]:


full_mean_df = full_df.groupby(level=0, axis=0).mean()
full_mean_df.tail()


# ### remove global mean

# In[16]:


clean_df = pd.DataFrame()
for i in full_df.index.levels[1]:
    this = full_df.xs(i, level=1)
    diff = this - full_mean_df
    
    diff.index = pd.MultiIndex.from_tuples([(j, i) for j in diff.index])
    
    clean_df = pd.concat([clean_df, diff])
    
clean_df = clean_df.sort_index()


# In[17]:


clean_df.tail()


# Example:

# In[18]:


plt.figure(figsize=(16,6), dpi=300)
sns.stripplot(data=clean_df.xs('Happy', level=1))
plt.xticks(rotation=90)
sns.despine()
plt.show()


# ### transform into long format

# In[19]:


long_df = clean_df.copy()
long_df.index.names = ['pnum', 'True']

long_df = pd.melt(long_df.reset_index(),
                  id_vars=['pnum', 'True'],
                  var_name=['AU-Emo', 'AU-Num'],
                  value_name='click count\n(deviation from global mean)'
                 )


# In[20]:


long_df.tail()


# ### this is to match AUs to Emotions
# and give them differen colors depending of the Emotion they belong to

# In[21]:


hue_order = []
hue_palette = []
for n, i in enumerate(my_order):
    if i != 'Neutral':
        hue_order.append(i)
        hue_palette.append(palette7[n])


# ## Plots

# ### all AUs averaged over emotions

# In[22]:


plt.figure(figsize=(16, 6))

sns.barplot(data=long_df, 
            x='True', 
            y='click count\n(deviation from global mean)', 
            hue='AU-Emo',
            order=my_order,
            palette=hue_palette,
            ci=my_ci,
            hue_order=hue_order
           )

plt.legend(loc=(1.01, 0.2))
sns.despine(bottom=True, trim=True)
plt.axhline(0, linestyle=':', color='k', linewidth=2)
#plt.ylim(0, 2)
plt.show()


# with individual data points

# In[23]:


plt.figure(figsize=(16, 6))

sns.barplot(data=long_df, 
            x='True', 
            y='click count\n(deviation from global mean)', 
            hue='AU-Emo',
            order=my_order,
            palette=hue_palette,
            ci=my_ci,
            hue_order=hue_order
           )

sns.stripplot(data=long_df, 
              x='True', 
              y='click count\n(deviation from global mean)', 
              hue='AU-Emo',
              order=my_order,
              dodge=True,
              palette=hue_palette,
              edgecolor='white',
              linewidth=1,
              hue_order=hue_order,
              alpha=0.2
             )

plt.legend(loc=(1.01, 0.2))
sns.despine(bottom=True, trim=True)
plt.axhline(0, linestyle=':', color='k', linewidth=2)
#plt.ylim(0, 2)
plt.show()


# ### split into individual AUs

# #### this is to get the colors right

# In[24]:


au_colors = []
au_order = []
for emo, au in full_df.loc[:, hue_order].columns:
    
    this_n = np.where(np.array(my_order) == emo)[0][0]
    this_color = palette7[this_n]
    au_colors.append(this_color)
    au_order.append(au)


# #### plot

# In[25]:


for emo in my_order:
    
    this_df = long_df[long_df.loc[:, 'True']==emo]
    
    plt.figure(figsize=(8, 6))
    sns.barplot(data=this_df, 
                x='AU-Num', 
                y='click count\n(deviation from global mean)',
                order=au_order,
                palette=au_colors
               )
    plt.xticks(rotation=90)
    sns.despine()
    plt.title(emo)
    plt.show()


# ## Make big plot

# ### differentiate between lower and upper AUs

# Get the number of the AU from the ```au_order``` list, by using a regex which searches for the first number

# In[26]:


au_order[0]


# In[27]:


regex = re.compile(r'AU(\d+).*')


# In[28]:


regex.findall( au_order[0] )


# Do for all and store in Dictionary

# In[29]:


d = {}
for i in au_order:
    d[i] = int(regex.findall(i)[-1])


# In[30]:


emo_list = full_df.loc[:, hue_order].columns.get_level_values(0)
emo_list


# ### Make plot

# In[31]:


fig = plt.figure(figsize=(16, 12), dpi=100)

for n, emo in enumerate(hue_order):

    ax = plt.subplot(2, 3, n+1)
    
    this_df = long_df[long_df.loc[:, 'True']==emo]
    
    sns.barplot(data=this_df, 
                y='AU-Num', 
                x='click count\n(deviation from global mean)',
                order=au_order,
                palette=au_colors,
                ci=my_ci,
                ax=ax,
               )

    this_alphas = [(0.1, 1)[i==emo] for i in emo_list]
    for bar, alpha in zip(ax.containers[0], this_alphas ):
        bar.set_alpha(alpha)
    
    if (n+3)%3!=0:
        ax.set_yticks([])
    else:
        ax.set_yticks(range(len(au_order)))
        ax.set_yticklabels(au_order, rotation=0)

    if n <3:
        ax.set_xlabel('')
    
    ax.axvline(0, linestyle=':', color='k', linewidth=2)

    ax.set_xlim(-1.0, 1.5)
    ax.set_ylabel('')    
    ax.tick_params(left=False)

    ax.set_title(emo, y=1)

    sns.despine(left=True, trim=True, offset=10)
    
    # give patterns to bars    
    for label, patch in zip(au_order, ax.patches):
        num = d[label]
        if num < 9: # AUs with numbers below 9 refer to the upper face half* (*there are also some above #40 but these are not used here, also AU9 could be considered to belong to both halves) 
            hatch = '/\\'
        else:
            hatch = 'O.'
        
        patch.set_hatch(hatch)

plt.tight_layout()
#plt.savefig('../reports/figures/au_fig.png', dpi=300, bbox_inches='tight')

plt.show()   


# In[32]:


fig = plt.figure(figsize=(16, 12), dpi=100)

for n, emo in enumerate(hue_order):

    ax = plt.subplot(2, 3, n+1)
    
    this_df = long_df[long_df.loc[:, 'True']==emo]
    
    sns.barplot(data=this_df, 
                y='AU-Num', 
                x='click count\n(deviation from global mean)',
                order=au_order,
                palette=au_colors,
                ci=my_ci,
                ax=ax,
               )

    #this_alphas = [(0.1, 1)[i==emo] for i in emo_list]
    #for bar, alpha in zip(ax.containers[0], this_alphas ):
    #    bar.set_alpha(alpha)
    
    if (n+3)%3!=0:
        ax.set_yticks([])
    else:
        ax.set_yticks(range(len(au_order)))
        ax.set_yticklabels(au_order, rotation=0)

    if n <3:
        ax.set_xlabel('')
    
    ax.axvline(0, linestyle=':', color='k', linewidth=2)

    ax.set_xlim(-1.0, 1.5)
    ax.set_ylabel('')    
    ax.tick_params(left=False)

    ax.set_title(emo, y=1.02, fontsize=18)

    sns.despine(left=True, trim=True, offset=10)
    
    # give patterns to bars    
    for label, patch in zip(au_order, ax.patches):
        num = d[label]
        if num < 9: # AUs with numbers below 9 refer to the upper face half* (*there are also some above #40 but these are not used here, also AU9 could be considered to belong to both halves) 
            hatch = '/\\'
        else:
            hatch = 'O.'
        
        patch.set_hatch(hatch)

plt.tight_layout()
plt.savefig('../reports/figures/au_fig.png', dpi=300, bbox_inches='tight')

plt.show()   


# ### check which AU is most important

# In[33]:


long_df.tail()


# average over participants

# In[34]:


mean_long_df = long_df.groupby(['True', 'AU-Emo', 'AU-Num']).mean()
mean_long_df.tail()


# reshape and remove neutral (because it has no AUs)

# In[35]:


mean_au_df = mean_long_df.unstack(0).T.loc['click count\n(deviation from global mean)', :]
mean_au_df = mean_au_df.drop('Neutral')
mean_au_df = mean_au_df.loc[hue_order, hue_order]


# In[36]:


mean_au_df.style.highlight_max(axis=1)


# ### for each emotion, which is the AU with the highest value?

# In[37]:


these_max = (mean_au_df.T == mean_au_df.max(axis=1)).idxmax()
these_max


# #### Do all match?

# In[38]:


(these_max.index == these_max.apply(lambda x:x[0])).all()

