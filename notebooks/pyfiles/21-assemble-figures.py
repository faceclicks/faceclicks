#!/usr/bin/env python
# coding: utf-8

# # Assemble Figures
# 
# This was too cumersome, so I ended up assembling the subplots in GIMP

# ### import modules

# In[1]:


import numpy as np

from matplotlib import patches

import matplotlib.pyplot as plt
import seaborn as sns


# ## Example from Matplotlib Manual
# 
# https://matplotlib.org/stable/tutorials/intermediate/arranging_axes.html

# In[2]:


fig = plt.figure()

gs = fig.add_gridspec(nrows=3, ncols=3, 
                      left=0, right=0.5,
                      hspace=0., wspace=0.0)

ax0 = fig.add_subplot(gs[0:2, 0:3])
ax0.text(0.5, 0.5, 'ax0')

ax1 = fig.add_subplot(gs[2, 0:2])
ax1.text(0.5, 0.5, 'ax1')

ax2 = fig.add_subplot(gs[2, 2])
ax2.text(0.5, 0.5, 'ax2')


# ## Answer Figure for different Emotions and Masking conditions

# In[3]:


im1 = '../reports/figures/ans_correct_fig.png'
im2 = '../reports/figures/ans_indi_fig.png'
im3 = '../reports/figures/ans_cms_fig.png'
im4 = '../reports/figures/ans_cm_diff_fig.png'


# In[4]:


fig = plt.figure(figsize=(16, 16))

gs = fig.add_gridspec(nrows=4, ncols=1, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[0, :])
ax1.imshow(plt.imread(im1))
ax1.set_title('A', x=0, y=0.95, fontsize=24)

ax2 = fig.add_subplot(gs[1, :])
ax2.imshow(plt.imread(im2))
ax2.set_title('B', x=0.02, y=0.95, fontsize=24)

ax3 = fig.add_subplot(gs[2, :])
ax3.imshow(plt.imread(im3))
ax3.set_title('C', x=0.02, y=0.95, fontsize=24)

ax4 = fig.add_subplot(gs[3, :])
ax4.imshow(plt.imread(im4))
ax4.set_title('D', x=0.02, y=0.95, fontsize=24)

#ax1.axis('off')
#ax2.axis('off')
#ax3.axis('off')
#ax4.axis('off')

plt.savefig('../reports/figures/merge_answer_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Clicks

# In[5]:


im1 = '../reports/figures/im_mask_baseline_fig.png'
im2 = '../reports/figures/im_mask_clean_fig.png'


# In[6]:


fig = plt.figure(figsize=(16, 19))

gs = fig.add_gridspec(nrows=2, ncols=1, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[0, :])
ax1.imshow(plt.imread(im1))
#ax1.axis('off')
ax1.set_title('A', x=0.01, y=0.95, fontsize=24)

ax2 = fig.add_subplot(gs[1, :])
ax2.imshow(plt.imread(im2))
#ax2.axis('off')
ax2.set_title('B', x=0.01, y=0.95, fontsize=24)

ax2.add_patch(patches.Rectangle((0 ,0), 7000, 90, color='white') )

plt.savefig('../reports/figures/merge_imstats_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ### other clicks version

# In[7]:


#im1 = '../reports/figures/im_mask_stats_fig.png'
#im2 = '../reports/figures/im_stats_fig.png'
#im3 = '../reports/figures/im_stats_fig.png'


# In[8]:


#fig = plt.figure(figsize=(16, 24))
#
#gs = fig.add_gridspec(nrows=10, ncols=14, 
#                      left=0, right=1,
#                      hspace=0., wspace=0.0)
#
#ax1 = fig.add_subplot(gs[:4, :])
#ax1.imshow(plt.imread(im1))
#ax1.axis('off')
#ax1.set_title('A', x=-0.075, y=0.4, fontsize=24)
#
#ax2 = fig.add_subplot(gs[4:, 1:7])
#ax2.imshow(plt.imread(im2))
#ax2.axis('off')
#ax2.set_title('B', x=-0.075, y=0.4, fontsize=24)
#
#ax3 = fig.add_subplot(gs[4:, 7:-1])
#ax3.imshow(plt.imread(im3))
#ax3.axis('off')
#ax3.set_title('C', x=-0.075, y=0.4, fontsize=24)
#
#plt.savefig('../reports/figures/merge_ims_fig.png', bbox_inches='tight', dpi=300)
#plt.show()


# ## Figure with Difference Upper-Lower Results

# In[9]:


im1 = '../reports/figures/click_diff_fig.png'
im2 = '../reports/figures/ans_diff_fig.png'
im3 = '../reports/figures/click_stat_fig.png'
im4 = '../reports/figures/ans_stat_fig.png'
im5 = '../reports/figures/compare_fig.png'


# In[10]:


fig = plt.figure(figsize=(16, 24))

gs = fig.add_gridspec(nrows=10, ncols=6, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[:3, :3])
ax1.imshow(plt.imread(im1))
ax1.set_title('A', x=-0.075, y=0.4, fontsize=24)

ax2 = fig.add_subplot(gs[:3, 3:])
ax2.imshow(plt.imread(im2))
ax2.set_title('B', x=-0.075, y=0.4, fontsize=24)

ax3 = fig.add_subplot(gs[3:6, :3])
ax3.imshow(plt.imread(im3))
ax3.set_title('C', x=-0.075, y=0.4, fontsize=24)

ax4 = fig.add_subplot(gs[3:6, 3:])
ax4.imshow(plt.imread(im4))
ax4.set_title('D', x=-0.075, y=0.4, fontsize=24)

ax5 = fig.add_subplot(gs[6:, 1:-1])
ax5.imshow(plt.imread(im5))
ax5.set_title('E', x=-0.075, y=0.4, fontsize=24)

#ax1.axis('off')
#ax2.axis('off')
#ax3.axis('off')
#ax4.axis('off')
#ax5.axis('off')

plt.savefig('../reports/figures/merge_diff_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# In[11]:


#fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 16))#, dpi=300)
#ax1.imshow(plt.imread(im1))
#ax2.imshow(plt.imread(im2))
#ax3.imshow(plt.imread(im3))
#ax4.imshow(plt.imread(im4))
#
#ax1.set_title('A', x=0.06, y=0.95, fontsize=72) # Coordinates are relative to subplot, not to figure
#ax2.set_title('B', x=0.0, y=0.95, fontsize=72)
#ax3.set_title('C', x=-0.2, y=0.95, fontsize=72)
#ax4.set_title('D', x=-0.2, y=0.95, fontsize=72)
#
#ax1.axis('off')
#ax2.axis('off')
#ax3.axis('off')
#ax4.axis('off')
#
#ax1.set_position([0,  -0.04,  0.5, 1.07 ])
#ax2.set_position([0.55,   0,  0.5, 1 ])
#
#ax3.set_position([0,    -0.2, 0.6, 0.4 ])
#ax4.set_position([0.55, -0.2, 0.6, 0.4 ])
#
#plt.savefig('../reports/figures/merge_diff_fig.png', bbox_inches='tight', dpi=300)
#plt.show()


# ## MDS Plots
# 
# White Rectangle to mask out redundant subplot titles

# In[12]:


im1 = '../reports/figures/mds_ans_fig.png'
im2 = '../reports/figures/mds_click_fig.png'
im3 = '../reports/figures/mds_im_fig.png'


# In[13]:


fig = plt.figure(figsize=(16, 16))

gs = fig.add_gridspec(nrows=3, ncols=1, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[0, :])
ax1.imshow(plt.imread(im1))
ax1.axis('off')
ax1.set_title('Answer', x=-0.075, y=0.4, fontsize=24)

ax2 = fig.add_subplot(gs[1, :])
ax2.imshow(plt.imread(im2))
ax2.add_patch(patches.Rectangle((0 ,0), 7000, 250, color='white') )
ax2.axis('off')
ax2.set_title('Click', x=-0.075, y=0.4, fontsize=24)

ax3 = fig.add_subplot(gs[2, :])
ax3.imshow(plt.imread(im3))
ax3.add_patch(patches.Rectangle((0 ,0), 7000, 220, color='white') )
ax3.axis('off')
ax3.set_title('Image', x=-0.075, y=0.4, fontsize=24)

plt.savefig('../reports/figures/merge_mds_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Action Units

# In[14]:


im1 = '../reports/figures/au_drawings.png'
im2 = '../reports/figures/au_fig.png'


# In[15]:


fig = plt.figure(figsize=(16, 14))

gs = fig.add_gridspec(nrows=13, ncols=15, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[:3, 1:])
ax1.imshow(plt.imread(im1))
ax1.axis('off')
ax1.set_title('A', x=-0.04, y=0.95, fontsize=24)

ax2 = fig.add_subplot(gs[3:, :])
ax2.imshow(plt.imread(im2))
ax2.axis('off')
ax2.set_title('B', x=0.02, y=0.95, fontsize=24)


plt.savefig('../reports/figures/merge_aus_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Dissimilarity Matrices

# In[16]:


im1 = '../reports/figures/diss_ans_fig.png'
im2 = '../reports/figures/diss_click_fig.png'
im3 = '../reports/figures/diss_im_fig.png'


# In[17]:


fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(16, 12))#, dpi=300)

ax1.imshow(plt.imread(im1))
ax2.imshow(plt.imread(im2))
ax3.imshow(plt.imread(im3))
#ax1.set_title('A', x=0.06, y=0.95, fontsize=72)
#ax2.set_title('B', x=0.0, y=0.95, fontsize=72)

ax1.axis('off')
ax2.axis('off')
ax3.axis('off')

#ax1.set_position([0, -0.04, 1, 1.07 ])
#ax2.set_position([1, 0, 1, 1 ])

plt.tight_layout()

plt.savefig('../reports/figures/merge_diss_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Predictions

# In[18]:


im1 = '../reports/figures/pred_acc_fig.png'
im2 = '../reports/figures/pred_indi_fig.png'
im3 = '../reports/figures/pred_compare_fig.png'


# In[19]:


fig = plt.figure(figsize=(16, 16))

gs = fig.add_gridspec(nrows=3, ncols=13, 
                      left=0, right=1,
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[0, :])
ax1.imshow(plt.imread(im1))
ax1.axis('off')
ax1.set_title('A', x=0, y=0.95, fontsize=24)

ax2 = fig.add_subplot(gs[1, 1:-2])
ax2.imshow(plt.imread(im2))
ax2.axis('off')
ax2.set_title('B', x=0, y=0.95, fontsize=24)

ax3 = fig.add_subplot(gs[2, 2:-2])
ax3.imshow(plt.imread(im3))
ax3.axis('off')
ax3.set_title('C', x=0, y=0.95, fontsize=24)

plt.savefig('../reports/figures/merge_pred_fig.png', bbox_inches='tight', dpi=300)
plt.show()

