#!/usr/bin/env python
# coding: utf-8

# # Visualize Main Image Statistics

# ### import packages

# In[1]:


import pickle
import json

import numpy as np

import pandas as pd
import cv2
from scipy import stats

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from matplotlib.colors import ListedColormap
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from matplotlib import animation
from IPython.display import HTML

from tqdm.notebook import tqdm


# In[2]:


#import sys
#sys.path.append('../helper/')
#from pixelate_rgb import pixelate_rgb, pixelate_bin


# ### get settings

# In[3]:


with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)

with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# In[4]:


with open('../data/interim/im_dims.json', 'r') as fp:
    dim_dict = json.load(fp)

n_rows = dim_dict['n_rows']
n_cols = dim_dict['n_cols']
new_n_rows = dim_dict['new_n_rows']
new_n_cols = dim_dict['new_n_cols']
scale_factor = dim_dict['scale_factor']


# ### get stimuli

# In[5]:


stim_df = pd.read_csv('../data/interim/stim_df.tsv', sep='\t', index_col=[0, 1, 2])
stim_df.tail()


# In[6]:


def load_im(file_name):
    
    img = cv2.imread(file_name,  cv2.IMREAD_GRAYSCALE)
    x_shape, y_shape = img.shape
    scale = 1
    new_shape = (int(y_shape*scale), int(x_shape*scale))
    res = cv2.resize(img, dsize=new_shape)
    
    return res


# ### average face

# In[7]:


biggest_im = np.loadtxt('../data/interim/biggest_im.txt')


# ### get height of dividing line

# In[8]:


orig_height = load_im(stim_df.iloc[0,0]).shape[0]
cuts = stim_df.loc[:, 'cut'].mean()
cut_ratio = cuts/orig_height
cut_ratio


# In[9]:


im_cut_height = int(biggest_im.shape[0]*cut_ratio)
im_cut_thick = int(stim_df.loc[:, 'cut'].std())
im_cut_height, im_cut_thick


# In[10]:


dim_dict['im_cut_height'] = im_cut_height
dim_dict['im_cut_thick'] = im_cut_thick


# In[11]:


with open('../data/interim/im_dims.json', 'w') as fp:
    json.dump(dim_dict, fp)


# ## Against Baseline

# In[12]:


with open('../data/processed/stat_ims.json', 'rb') as fp:
    d = pickle.load(fp)


# In[13]:


a = 'Full_gr_Half'


# In[14]:


def make_baseline_plot(fig, d, comparison, settings_dict=settings_dict, my_order=my_order, im_cut_height=im_cut_height):
    
    count = 1
    
    conds = ['Full', 'Half', 'Full_gr_Half']
    for j, cond in enumerate(conds):
    
        for n, emo in enumerate(my_order):

            ax = plt.subplot(len(conds), len(my_order), count)
            ax.imshow(biggest_im, cmap='Greys_r', alpha=0.7)

            im = d[cond][comparison][f'{emo}-{emo}']

            imax = ax.imshow(im, 
                      cmap=ListedColormap(color_palette),
                      vmin=-1,
                      vmax=1,
                     )

            count += 1
            
            sns.despine(bottom=True, left=True)
            ax.set_xticks([]); ax.set_yticks([])
            
            if j == 0:
                ax.set_title(emo, fontsize=16)

            if n==0:
                my_title = ((' '*5) + cond.replace('_gr_', ' > '))[-12:]
                ax.set_ylabel(my_title, rotation=0, fontsize=16, labelpad=50)

            if j>0:
                ax.axhline(im_cut_height, color='k', linewidth=3, alpha=1)

    plt.tight_layout()

    axins1 = inset_axes(
        ax,
        width="100%",  # width: 50% of parent_bbox width
        height="100%",  # height: 5%
        loc='lower right', 
        #borderpad=0,
        bbox_to_anchor=(1220, -100, 2800, 100), # xpos, ypos, width, heigth
        
    )
    fig.colorbar(imax, 
                 cax=axins1, 
                 orientation="horizontal", 
                 ticks=np.arange(-1,1.1,0.2),
                )

    return fig


# In[15]:


for comparison in ['baseline', 'clean']:
    fig = plt.figure(figsize=(16, 8.5), dpi=300)
    fig = make_baseline_plot(fig, d, comparison, settings_dict=settings_dict)
    plt.savefig(f'../reports/figures/im_mask_{comparison}_fig.png', dpi=300, bbox_inches='tight')
    plt.show()


# ## descriptive plots for individual identities

# Get big data

# In[16]:


with open('../data/interim/biggest_Full_flat_df.pkl', 'rb') as fp:
    big_df = pickle.load(fp)


# Average by Face Identity

# In[17]:


face_ident_df = big_df.groupby(level=[0, 1, 2]).mean()


# Full Condition Only

# In[18]:


full_stim_df = stim_df.loc['Full']


# In[19]:


def pixelate_greyscale(img, window):
    """
    modified from: https://stackoverflow.com/a/63627054 CC BY-SA 4.0 Aray Karjauv 
    """
    
    n, m = img.shape
    n, m = n - n % window, m - m % window
    img1 = np.zeros((n, m))
    
    for x in range(0, n, window):
        for y in range(0, m, window):
            img1[x:x+window,y:y+window] = img[x:x+window,y:y+window].mean(axis=(0, 1))
            
    return img1


# In[20]:


idents = face_ident_df.index.levels[2]

fig = plt.figure(figsize=(16, 14))

count = 1
for m, emo in enumerate(tqdm(my_order)):

    for n, i in enumerate(tqdm(idents, leave=False)):

        ax = plt.subplot(len(my_order), len(idents), count)

        bg_file = full_stim_df.loc[(emo, i), "file_name"]
        bg_im = cv2.imread(bg_file, cv2.IMREAD_GRAYSCALE)
        bg_im = pixelate_greyscale(bg_im / 255.0, 40)

        mean_data = face_ident_df.loc[(emo, emo, i)]
        mean_data = mean_data * (abs(mean_data) > 0.1)
        mean_thresh_data = mean_data.replace(0, np.nan)
        mean_thresh_im = mean_thresh_data.values.reshape(new_n_rows, new_n_cols)

        ax.imshow(bg_im[::scale_factor, ::scale_factor], cmap="Greys_r", vmin=0.1, vmax=1)
        ax.imshow(mean_thresh_im, cmap=ListedColormap(color_palette), vmin=-1, vmax=1)

        if m == 0:
            ax.set_title(i, fontsize=16)

        if n == 0:
            my_title = ((" " * 5) + emo)[-12:]
            ax.set_ylabel(my_title, rotation=0, fontsize=16, labelpad=45)

        count += 1

        sns.despine(bottom=True, left=True)
        ax.set_xticks([])
        ax.set_yticks([])

plt.tight_layout()
plt.savefig(f"../reports/figures/im_all_ident_baseline_fig.png", dpi=300, bbox_inches="tight")
plt.show()


# Remove global mean for each pixel

# In[21]:


demean_df = face_ident_df.T.apply(lambda x: x - face_ident_df.mean()).T


# In[22]:


idents = demean_df.index.levels[2]

fig = plt.figure(figsize=(16, 14))

count = 1
for m, emo in enumerate(tqdm(my_order)):

    for n, i in enumerate(tqdm(idents, leave=False)):

        ax = plt.subplot(len(my_order), len(idents), count)

        bg_file = full_stim_df.loc[(emo, i), "file_name"]
        bg_im = cv2.imread(bg_file, cv2.IMREAD_GRAYSCALE)
        bg_im = pixelate_greyscale(bg_im / 255.0, 40)

        mean_data = demean_df.loc[(emo, emo, i)]
        mean_data = mean_data * (abs(mean_data) > 0.1)
        mean_thresh_data = mean_data.replace(0, np.nan)
        mean_thresh_im = mean_thresh_data.values.reshape(new_n_rows, new_n_cols)

        ax.imshow(bg_im[::scale_factor, ::scale_factor], cmap="Greys_r", vmin=0.1, vmax=1)
        ax.imshow(mean_thresh_im, cmap=ListedColormap(color_palette), vmin=-1, vmax=1)

        if m == 0:
            ax.set_title(i, fontsize=16)

        if n == 0:
            my_title = ((" " * 5) + emo)[-12:]
            ax.set_ylabel(my_title, rotation=0, fontsize=16, labelpad=50)

        count += 1

        sns.despine(bottom=True, left=True)
        ax.set_xticks([])
        ax.set_yticks([])

plt.tight_layout()
plt.savefig(f"../reports/figures/im_all_ident_clean_fig.png", dpi=300, bbox_inches="tight")
plt.show()


# ### for all conditions

# In[23]:


for cond in ['Full', 'Upper', 'Lower']:

    with open(f'../data/interim/biggest_{cond}_flat_df.pkl', 'rb') as fp:
        big_df = pickle.load(fp)

    face_ident_df = big_df.groupby(level=[0, 1, 2]).mean()
    cond_stim_df = stim_df.loc[cond]
    demean_df = face_ident_df.T.apply(lambda x: x - face_ident_df.mean()).T

    idents = face_ident_df.index.levels[2]

    print(cond)

    for emo in my_order:

        fig = plt.figure(figsize=(16, 3))

        for n, i in enumerate(idents):

            ax = plt.subplot(1, len(idents), n+1)

            bg_file = full_stim_df.loc[(emo, i), "file_name"]
            bg_im = cv2.imread(bg_file, cv2.IMREAD_GRAYSCALE)
            bg_im = pixelate_greyscale(bg_im / 255.0, 40)

            mean_data = demean_df.loc[(emo, emo, i)]
            mean_data = mean_data * (abs(mean_data)>0.1)
            mean_thresh_data = mean_data.replace(0, np.nan)
            mean_thresh_im = mean_thresh_data.values.reshape(new_n_rows, new_n_cols)

            ax.imshow(bg_im[::scale_factor, ::scale_factor], cmap="Greys_r", vmin=0.1, vmax=1)
            ax.imshow(mean_thresh_im, cmap=ListedColormap(color_palette), vmin=-1, vmax=1)

            ax.set_axis_off()
            ax.set_title(i)

        plt.suptitle(emo)
        plt.show()


# #### proportion of identites with at least certain value per pixel
# 
# i.e. thresholding all pictures at certain point and looking how many pass that threshold

# In[24]:


count_demean_df = (demean_df/abs(demean_df)) * (abs(demean_df)>0.2)
face_ident_sum_df = count_demean_df.groupby(level=[0, 1]).mean()


# In[25]:


fig = plt.figure(figsize=(16, 3))

for n, emo in enumerate(my_order):

    ax = plt.subplot(1, len(my_order), n+1)

    mean_data = face_ident_sum_df.loc[(emo, emo)]
    mean_thresh_data = mean_data.replace(0, np.nan)
    mean_thresh_im = mean_thresh_data.values.reshape(new_n_rows, new_n_cols)

    ax.imshow(biggest_im, cmap='Greys_r')
    axim = ax.imshow(mean_thresh_im, cmap=ListedColormap(color_palette), vmin=-1, vmax=1)
    
    ax.set_axis_off()
    ax.set_title(emo)

    plt.colorbar(axim, shrink=0.7)
    
plt.tight_layout()
plt.show()


# ### similiarity of individual identity patterns

# Correlate, only for correct answers

# In[26]:


clean_ident_df = pd.DataFrame()
for emo in my_order:
    this_df = face_ident_df.loc[emo].loc[[emo]]
    clean_ident_df = pd.concat([clean_ident_df, this_df])
corr_df = clean_ident_df.T.corr()


# Find maximal correlation

# In[27]:


mask = corr_df.round(10).replace(1, 0)
mask = (mask != mask.max()).T


# In[28]:


my_min, my_max = corr_df.min().min() , corr_df.max().max()

plt.figure(figsize=(32, 32))

sns.heatmap(corr_df, alpha=0.3, square=True, cmap=color_palette, vmin=my_min, vmax=my_max, cbar=False)
sns.heatmap(corr_df, mask=mask, square=True, cmap=color_palette, linewidth=1, vmin=my_min, vmax=my_max)

for i in range(0, 71, 10):
    plt.axhline(i, color='grey', linewidth=2)
    plt.axvline(i, color='grey', linewidth=2)

plt.show()


# How do the click patterns for the individual face identities cluster?

# In[29]:


sns.clustermap(corr_df, 
               square=True, 
               cmap=color_palette, vmin=my_min, vmax=my_max,
               figsize=(32, 32))


# ## interactive plot

# #### for full faces

# In[30]:


stat_df = pd.read_csv(f'../data/processed/stat_Full_clean_df.tsv', sep='\t', index_col=[0, 1, 2])
stat_df.tail()


# In[31]:


def make_thresh_im(this_df, p=settings_dict['p'], n=20, new_n_rows=new_n_rows, new_n_cols=new_n_cols):
    
    mean_data = this_df .loc['mean', :]
    tp_data = this_df.loc['tp', :]
    n_data = this_df.loc['n df', :]
        
    thresh_data = mean_data * (abs(tp_data)<p)
    thresh_data = thresh_data * (n_data>n)
    thresh_data = thresh_data.replace(0, np.nan)
    im = thresh_data.values.reshape(new_n_rows, new_n_cols)
    
    return im


# In[32]:


sample_n = stat_df.xs('sample n', level=2).max().max()


# In[ ]:


fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = plt.subplots(1, 7, figsize=(20, 4), dpi=100)
ax_tuple = (ax1, ax2, ax3, ax4, ax5, ax6, ax7)

def animate(i):

    my_max = stat_df.xs('mean', level=2).max().max()
    
    for n, (ax, emo) in enumerate(zip(ax_tuple, my_order)):
        
        ax.clear()
        
        this_p = 1 - stats.t.cdf(i, df=sample_n-1)
        
        this_df = stat_df.loc[(emo, emo), :]
        stat_im = make_thresh_im(this_df, p=this_p)

        ax.imshow(biggest_im, cmap='Greys_r', alpha=0.7)
        ax.imshow(stat_im, vmin=-my_max, vmax=my_max, cmap=ListedColormap(color_palette), alpha=1)
        
        ax.set_xticks([]); ax. set_yticks([]);
        ax.set_title(emo)
        ax.set_axis_off()
        
    fig.suptitle(f'thresh={i:.1f}')
    plt.tight_layout()

ani = animation.FuncAnimation(fig, animate, frames=np.arange(0, 10, 0.1), repeat=False);


# In[34]:


my_html = ani.to_jshtml()


# In[ ]:


HTML(my_html)


# In[36]:


with open('../reports/figures/main_fig.html', 'w') as fp:
    fp.write(my_html)


# ### interactive plot using t-values directly for thresholding
# (because too low p-values do not work with the floating point precision)

# In[37]:


def make_thresh_im(this_df, t, n=20, new_n_rows=new_n_rows, new_n_cols=new_n_cols):
    
    mean_data = this_df .loc['mean', :]
    t_data = this_df.loc['t', :]
    n_data = this_df.loc['n df', :]
        
    thresh_data = mean_data * (abs(t_data)>t)
    thresh_data = thresh_data * (n_data>n)
    thresh_data = thresh_data.replace(0, np.nan)
    im = thresh_data.values.reshape(new_n_rows, new_n_cols)
    
    return im


# In[ ]:


fig, (ax1, ax2, ax3, ax4, ax5, ax6, ax7) = plt.subplots(1, 7, figsize=(20, 4), dpi=100)
ax_tuple = (ax1, ax2, ax3, ax4, ax5, ax6, ax7)

def animate(i):

    my_max = stat_df.xs('mean', level=2).max().max()
    
    for n, (ax, emo) in enumerate(zip(ax_tuple, my_order)):
        
        ax.clear()
        
        this_t = i
        
        this_df = stat_df.loc[(emo, emo), :]
        stat_im = make_thresh_im(this_df, t=this_t)

        ax.imshow(biggest_im, cmap='Greys_r', alpha=0.7)
        ax.imshow(stat_im, vmin=-my_max, vmax=my_max, cmap=ListedColormap(color_palette), alpha=1)
        
        ax.set_xticks([]); ax. set_yticks([]);
        ax.set_title(emo)
        ax.set_axis_off()
        
    fig.suptitle(f'thresh={i:.1f}')
    plt.tight_layout()

ani = animation.FuncAnimation(fig, animate, frames=np.arange(0, 20, 0.2), repeat=False);


# In[39]:


my_html = ani.to_jshtml()


# In[40]:


HTML(my_html)


# In[41]:


with open('../reports/figures/main_t_fig.html', 'w') as fp:
    fp.write(my_html)

