#!/usr/bin/env python
# coding: utf-8

# # Analyse Answer Counts

# ### import modules

# In[1]:


import pickle
import json

import numpy as np
import pandas as pd

from scipy import stats
from sklearn import metrics

import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import seaborn as sns
sns.set_context('talk')

from matplotlib.offsetbox import OffsetImage, AnnotationBbox


# ### get data

# In[2]:


data_df = pd.read_csv('../data/interim/clean_big_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])
data_df.tail()


# ### get only answers 
# 
# 7-way decision on how a picture was labeled

# In[3]:


data_df = data_df.xs('Answer', level=1, axis=1)
data_df.tail()


# Check number of answers given

# In[4]:


data_df.groupby(level=0, axis=1).count().min()


# ### set order of emotions

# This hand-crafted order seems to best capture the upper-lower difference across different metrics

# In[5]:


my_order = ['Happy', 'Disgusted', 'Neutral', 'Angry', 'Sad', 'Fearful', 'Surprised']


# ### set significance level

# * 0.0000003 -> five sigma

# In[6]:


num_sigma = 5
p_five_sigma = 1 - stats.norm.cdf(num_sigma)
ci_five_sigma = (1 - p_five_sigma) * 100


# ### set guessing rate

# In[7]:


n_choices = len(my_order)
ans_count = data_df.groupby(level=[0,1], axis=1).count()
assert ans_count.max().max() == ans_count.min().min()
n_answers = float(ans_count.max().max())

chance_rate = 1/n_choices
chance_rate


# ### bootstraps for CIs

# In[8]:


n_bootstraps = 10000


# #### store for consistent re-use

# In[9]:


settings_dict = {'my_order': my_order,
                 'sigma': num_sigma,
                 'p': p_five_sigma,
                 'ci': ci_five_sigma,
                 'boots': n_bootstraps,
                 'guess': chance_rate,
                 'n_answers': n_answers
                }


# In[10]:


with open('../references/my_settings.json', 'w') as fp:
    json.dump(settings_dict, fp)


# ### set guessing rate

# ### make inferential stats
# 
# Returns a binary mask which removes all values below five sigma

# In[11]:


def make_stat_mask(in_df, chance_rate, p_five_sigma, my_order=my_order):
    
    df = in_df.copy()
    df = df - chance_rate
    
    stat_df = pd.DataFrame(df.apply(lambda x:stats.ttest_1samp(x, 0) if x.std()!=0 else 99), columns=df.columns).T

    mask = pd.DataFrame(stat_df[1]).unstack(0)[1].loc[my_order, my_order] > p_five_sigma
    
    return mask


# ### make confusion matrix for one condition

# In[12]:


def make_cm_df(cond, p_num, data_df, my_order=my_order):
    
    y_pred = data_df.loc[p_num, cond]
    y_true = data_df.loc[:, cond].apply(lambda x:x.name[0])
    
    cm = metrics.confusion_matrix(y_true, y_pred, labels=my_order)
    
    cm_df = pd.DataFrame(cm, index=my_order, columns=my_order)
    cm_df.index.name = 'True'
    cm_df.columns.name = 'Answer'
    
    return cm_df


# get example participant

# In[13]:


p_num = data_df.index[3]
p_num


# In[14]:


cond_cm_df = make_cm_df('Full', p_num, data_df)
cond_cm_df


# ### make confusion matrices for all conditions of one participant

# In[15]:


def make_p_cm_df(p_num, data_df):
    
    big_cm_df = pd.DataFrame()
    
    for cond in data_df.columns.levels[0]:
        
        cond_cm_df = make_cm_df(cond, p_num, data_df)
        cond_cm_df.columns = pd.MultiIndex.from_tuples([(cond,c) for c in cond_cm_df.columns])
        
        big_cm_df = pd.concat([big_cm_df, cond_cm_df], axis=1)
        
    return big_cm_df


# In[16]:


p_cm_df = make_p_cm_df(p_num, data_df)
p_cm_df


# ## Visualize

# ### get colormaps

# In[17]:


with open('../models/red_palette.pkl', 'rb') as fp:
    red_palette = pickle.load(fp)
    
with open('../models/blue_palette.pkl', 'rb') as fp:
    blue_palette = pickle.load(fp)
    
with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/primary_palette.pkl', 'rb') as fp:
    primary_palette = pickle.load(fp)
    
with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)

sns.palplot(palette7)


# ### show confusions as heatmaps - for one participant

# In[18]:


def cm_plots(df, fig, my_order=my_order, make_mask=False):
    
    conds = df.columns.levels[0]
    for n, c in enumerate(conds):

        cm_df = df.loc[:, c]
        cm_df = cm_df.loc[my_order, my_order]
        
        ax = plt.subplot(1, len(conds), n+1)

        sns.heatmap(cm_df, 
                    annot=True, 
                    cmap=blue_palette, 
                    square=True, 
                    cbar=False, 
                    fmt='.2f', 
                    ax=ax)
        
        ax.set_ylabel('')
        ax.tick_params(left=False, bottom=False)
        ax.set_title(c, y=1.05, fontsize=24)
        if n > 0:
            ax.set_yticks([])

    plt.tight_layout()
    
    return fig


# In[19]:


fig = plt.figure(figsize=(20, 7), dpi=100)
cm_df = cm_plots(p_cm_df, fig)
plt.show()


# ### compare confusions across conditions

# In[20]:


def cm_diff_plots(df, fig, mask_dict=False, my_order=my_order):
    
    conds = df.columns.levels[0]

    a = []
    n = 0
    
    for c1 in conds:

        for c2 in conds:

            if c1 != c2 and set((c1, c2)) not in a:

                cm_df = df.loc[:, c1] - df.loc[:, c2]
                cm_df = cm_df.loc[my_order, my_order]
                
                
                if type(mask_dict) == dict:
                    mask = mask_dict[c]
                else:
                    mask = False
                
                ax = plt.subplot(1, len(conds), n+1)

                sns.heatmap(cm_df, 
                            annot=True, 
                            cmap=color_palette, 
                            square=True, 
                            cbar=False, 
                            vmin=-1,
                            vmax=1,
                            fmt='.2f', 
                            ax=ax)
                if n > 0:
                    ax.set_yticks([])
                ax.set_ylabel('')
                ax.set_title(f'{c1}-{c2}', y=1.05, fontsize=24)    
                
                a.append(set((c1, c2)))
                n+=1

    plt.tight_layout()
    
    return fig


# In[21]:


fig = plt.figure(figsize=(20, 7), dpi=100)
cm_df = cm_diff_plots(p_cm_df/n_answers, fig)
plt.show()


# True should sum up to 10

# In[22]:


p_cm_df.groupby(level=0,axis=1).sum()


# ### make cm for all participants

# In[23]:


def make_big_cm_df(data_df):
    
    big_cm_df = pd.DataFrame()
    
    for p_num in data_df.index:
        
        sum_df = make_p_cm_df(p_num, data_df)

        # wide format, with each participant in one row
        sum_df = pd.DataFrame(sum_df.unstack()).T
        sum_df.index = [p_num]
        
        big_cm_df = pd.concat([big_cm_df, sum_df], sort=True)
        
    return big_cm_df


# In[24]:


big_cm_df = make_big_cm_df(data_df) / n_answers # make into proportion
big_cm_df.tail()


# #### store for later re-use

# In[25]:


big_cm_df.to_csv('../data/processed/big_cm_df.tsv', sep='\t')


# check how often someone has no used a certain label at all

# In[26]:


fear_answers = big_cm_df.xs('Fearful', level=1, axis=1)
fear_sum_answers = fear_answers.groupby(level=0, axis=1).sum()

fear_sum_answers.sort_values(by='Lower').head(10)


# In[27]:


(fear_sum_answers==0).sum()


# ## Make Confusion Matrices at Group Level

# ### average over all participants

# In[28]:


def make_mean_cm_df(big_cm_df):
    
    mean_cm_df = pd.DataFrame(big_cm_df.mean()).unstack(1)[0]
    mean_cm_df = mean_cm_df.unstack(0).reorder_levels([1, 0], axis=1).sort_index(axis=1)
    
    return mean_cm_df


# In[29]:


mean_cm_df = make_mean_cm_df(big_cm_df)
mean_cm_df.tail()


# Rows ("True") must sum up to 10:

# In[30]:


check_sum_df = mean_cm_df.groupby(level=0, axis=1).sum()
check_sum_df


# In[31]:


assert (check_sum_df.round(14) == 1).all().all(), 'do proportions sum up to 1?'


# ## Plot group confusion matrices

# ### raw answers

# In[32]:


def cm_plots(big_df, fig, my_order=my_order, make_mask=False, chance_rate=chance_rate, p_five_sigma=p_five_sigma):
    
    df = make_mean_cm_df(big_df)
    
    conds = df.columns.levels[0]
    for n, c in enumerate(conds):

        cm_df = df.loc[:, c]
        cm_df = cm_df.loc[my_order, my_order]

        if make_mask:
            mask = make_stat_mask(big_df.loc[:, c], chance_rate, p_five_sigma)
        else:
            mask=False
        
        ax = plt.subplot(1, len(conds), n+1)

        sns.heatmap(cm_df, 
                    annot=True, 
                    cmap=blue_palette, 
                    square=True, 
                    cbar=False, 
                    fmt='.0%', 
                    alpha=0.3,
                    annot_kws={'alpha':0.3},
                    mask=mask!=mask,
                    ax=ax)
        
        sns.heatmap(cm_df, 
                    annot=True, 
                    cmap=blue_palette, 
                    square=True, 
                    cbar=False, 
                    fmt='.0%', 
                    mask=mask,
                    ax=ax)
        
        ax.set_ylabel('')
        ax.tick_params(left=False, bottom=False)
        ax.set_title(c, y=1.05, fontsize=24)
        if n > 0:
            ax.set_yticks([])

    plt.tight_layout()
    plt.savefig('../reports/figures/ans_cms_fig.png', bbox_inches='tight', dpi=300)

    return fig


# In[33]:


fig = plt.figure(figsize=(20, 7), dpi=100)
cm_df = cm_plots(big_cm_df, fig, make_mask=True)
plt.show()


# ### differences in answers

# In[34]:


def cm_diff_plots(big_df, fig, chance_rate=0, make_mask=False, my_order=my_order):
 
    df = make_mean_cm_df(big_df)
    conds = df.columns.levels[0]

    a = []
    n = 0
    
    for c1 in conds:

        for c2 in conds:

            if c1 != c2 and set((c1, c2)) not in a:

                cm_df = df.loc[:, c1] - df.loc[:, c2]
                cm_df = cm_df.loc[my_order, my_order]
                
                if make_mask:
                    big_diff_df = big_df.loc[:, c1] - big_df.loc[:, c2]
                    mask = make_stat_mask(big_diff_df, chance_rate, p_five_sigma)
                else:
                    mask=False
                
                ax = plt.subplot(1, len(conds), n+1)

                sns.heatmap(cm_df, 
                            annot=True, 
                            cmap=color_palette, 
                            square=True, 
                            cbar=False, 
                            fmt='.0%', 
                            vmin=-1,
                            vmax=1,
                            alpha=0.3,
                            mask=mask!=mask,
                            annot_kws={'alpha':0.3},
                            ax=ax)
                
                sns.heatmap(cm_df, 
                            annot=True, 
                            cmap=color_palette, 
                            square=True, 
                            cbar=False, 
                            vmin=-1,
                            vmax=1,
                            fmt='.0%', 
                            mask=mask,
                            ax=ax)
        
                if n > 0:
                    ax.set_yticks([])
                ax.set_ylabel('')
                ax.tick_params(left=False, bottom=False)
                ax.set_title(f'{c1}-{c2}', y=1.05, fontsize=24)    
                
                a.append(set((c1, c2)))
                n+=1

    plt.tight_layout()
    plt.savefig('../reports/figures/ans_cm_diff_fig.png', bbox_inches='tight', dpi=300)

    return fig


# In[35]:


fig = plt.figure(figsize=(20, 7), dpi=100)
cm_df = cm_diff_plots(big_cm_df, fig, make_mask=True)
plt.show()


# ## Only diagonal (correct answers)

# In[36]:


def make_big_correct_df(big_cm_df):
    
    big_correct_df = pd.DataFrame()

    for cond in big_cm_df.columns.levels[0]:
        cond_cm_df = big_cm_df.loc[:, cond]

        for c in cond_cm_df.columns.levels[0]:
            correct_df = cond_cm_df.loc[:, (c, c)]
            correct_df.name = (cond, c)
            big_correct_df = pd.concat([big_correct_df, correct_df], axis=1)

    big_correct_df.columns = pd.MultiIndex.from_tuples(big_correct_df)
    big_correct_df = big_correct_df.sort_index(axis=1)
    
    return big_correct_df


# In[37]:


big_correct_df = make_big_correct_df(big_cm_df)


# In[38]:


big_correct_df.tail()


# #### store for re-use

# In[39]:


big_correct_df.to_csv('../data/processed/big_correct_df.tsv', sep='\t')


# ## Visualize group level results

# ### set up color palettes

# Use three colors for three masking conditions

# In[40]:


palette3 = primary_palette[:3]
sns.palplot(palette3)


# ### set up images for visualizing emotions

# In[41]:


with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)


# ### reshape data

# In[42]:


long_correct_df = pd.melt(big_correct_df.reset_index(), 
                          id_vars='index', 
                          var_name=['cond', 'emotion'], 
                          value_name='% correct')
long_correct_df


# ### visualize 

# In[43]:


fig, ax = plt.subplots(1,1, figsize=(16, 6))

sns.barplot(data=long_correct_df, 
            x='emotion', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=0,
            ax=ax)

# add jitter to plot swarms
jitter_df = long_correct_df.copy()
jitter_df.loc[:, ['% correct']] = jitter_df.loc[:, ['% correct']].applymap(lambda x:x + (np.random.rand()-0.5)*0.02)

sns.swarmplot(data=jitter_df, 
            x='emotion', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=2,
            alpha=0.8,
            dodge=True,
            zorder=2,
            ax=ax)

sns.barplot(data=long_correct_df, 
            x='emotion', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=n_bootstraps, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

nconds = len(set(long_correct_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)
plt.xticks(y=-0.05)

plt.axhline(chance_rate, linewidth=2, linestyle=':', color='k')

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.15)
    ab = AnnotationBbox(imagebox, (x, y+0.04), pad=0, zorder=1)
    ax.add_artist(ab)
    
ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=22)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
plt.savefig('../reports/figures/ans_correct_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ## Make difference between clicks for upper and lower masking condition

# In[44]:


big_diff_df = big_correct_df.loc[:, 'Upper'] - big_correct_df.loc[:, 'Lower']
big_diff_df.tail()


# #### store for re-use

# In[45]:


big_diff_df.to_csv('../data/processed/ans_diff_correct_df.tsv', sep='\t')


# #### make long format for seaborn

# In[46]:


long_diff_df = pd.melt(big_diff_df.reset_index(), 
                       id_vars='index', 
                       var_name='emotion', 
                       value_name='Upper-Lower')


# ### barplot with pointswarms

# In[47]:


fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi=100)

plt.axhline(0, c='k', linestyle=':')

sns.barplot(data=long_diff_df, 
            x='emotion', 
            y='Upper-Lower', 
            order=my_order, 
            palette=palette7,
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=0,
            ax=ax)

jitter_df = long_diff_df.copy()
jitter_df.loc[:, ['Upper-Lower']] = jitter_df.loc[:, ['Upper-Lower']].applymap(lambda x:x + (np.random.rand()-0.5)*0.04)

sns.swarmplot(data=jitter_df, 
              x='emotion', 
              y='Upper-Lower', 
              order=my_order, 
              palette=palette7,
              linewidth=0.5,
              edgecolor='white',
              size=3,
              alpha=0.8,
              zorder=2,
              ax=ax)

sns.barplot(data=long_diff_df, 
            x='emotion', 
            y='Upper-Lower', 
            order=my_order, 
            palette=palette7,
            errwidth=5,
            n_boot=n_bootstraps,
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

plt.xticks(y=-0.05)
plt.xlabel('')

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.1)
    ab = AnnotationBbox(imagebox, (x, -1.0), pad=0, zorder=1)
    ax.add_artist(ab)

# abbreviated ticks
ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()])
plt.xticks(y=-0.06)

plt.ylabel('')
plt.ylim(-1, 1)
ax.set_yticks(np.arange(-1.0, 1.1, .2))
sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.savefig('../reports/figures/ans_diff_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ### make animation plot

# In[48]:


from matplotlib import animation
from IPython.display import HTML


# In[49]:


p_nums = np.unique( long_diff_df.loc[:, 'index'] )


# In[ ]:


fig, ax = plt.subplots(1, 1, figsize=(16, 6))

def animate(i):
    
    ax.clear()
    ax.set_title(f'sample size = {i}') 

    this_big_df = big_diff_df.drop(p_nums[i:]) 
    this_df = pd.melt(this_big_df.reset_index(), 
                           id_vars='index', 
                           var_name='emotion', 
                           value_name='Upper-Lower')

    my_order = this_big_df.mean().sort_values().index


    sns.barplot(data=this_df, 
                x='emotion', 
                y='Upper-Lower', 
                order=my_order, 
                palette=palette7,
                #capsize=.1,
                errwidth=5,
                n_boot=1000,
                ci=settings_dict['ci'],
                #alpha=0,
                ax=ax)

    jitter_df = this_df.copy()
    jitter_df.loc[:, ['Upper-Lower']] = jitter_df.loc[:, ['Upper-Lower']].applymap(lambda x:x + (np.random.rand()-0.5)*0.03)

    sns.swarmplot(data=jitter_df, 
                  x='emotion', 
                  y='Upper-Lower', 
                  order=my_order, 
                  palette=palette7,
                  linewidth=0.5,
                  edgecolor='grey',
                  size=3,
                  alpha=0.5,
                  zorder=99,
                  ax=ax)

    ax.axhline(0, c='k', linestyle=':')
    sns.despine(bottom=True, ax=ax)
    plt.legend([])
    
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))
    
    ax.set_ylim(-.8, .8)

ani = animation.FuncAnimation(fig, animate, frames=range(1, len(p_nums)+1), repeat=False);


# In[ ]:


my_html = ani.to_jshtml()


# In[52]:


HTML(my_html)

