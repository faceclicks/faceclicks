#!/usr/bin/env python
# coding: utf-8

# # Individual Image Analyses
# 
# Compare answers for different face identities

# ### import modules

# In[1]:


import json
import pickle

import numpy as np
import pandas as pd

from sklearn import preprocessing, manifold

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')


# ### get settings

# In[2]:


with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']

with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)


# ### get answer data

# In[3]:


ans_data_df = pd.read_csv('../data/interim/clean_big_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])
ans_data_df.tail()


# Only labels with Answers given

# In[4]:


ans_df = ans_data_df.xs('Answer', level=1, axis=1)
ans_df.tail()


# ### get prediction data

# In[5]:


pred_data_df = pd.read_csv('../data/processed/big_pred_CUTUP_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])
pred_data_df.tail()


# In[6]:


# irrespective of answer
pred_df = pred_data_df.groupby(level=[0, 1, 3], axis=1).first()
# only image prediction
pred_df =pred_df.applymap(lambda x:eval(x)[0] if x != None else None)
pred_df.tail()


# ### make count of how often the true emo was chosen

# In[7]:


def make_true_df(cond, ans_df, my_order=my_order):

    cond_df = ans_df.loc[:, cond]

    d = {}

    for true_emo, indi in cond_df.columns:

        these_data = cond_df.loc[:, (true_emo, indi)]

        for emo in my_order:
            
            correct_data = these_data == emo
            d[(true_emo, emo, indi)] = correct_data.mean()

    true_df = pd.DataFrame(d, index=['mean']).T.unstack(1)['mean']
    true_df = true_df.loc[my_order, my_order]
    
    return true_df


# #### check if answers sum up to 1

# For answers

# In[8]:


for cond in ans_df.columns.levels[0]:
    this_df = make_true_df(cond, ans_df)
    print(cond, this_df.sum(axis=1).min(), this_df.sum(axis=1).max() )


# For predictions

# In[9]:


for cond in pred_df.columns.levels[0]:
    this_df = make_true_df(cond, pred_df)
    print(cond, this_df.sum(axis=1).min(), this_df.sum(axis=1).max() )


# ### plot for all masking conditions

# In[10]:


def make_ax(df, n, ax):
    
    this_df = make_true_df(cond, df)

    sns.heatmap(this_df.T, square=True, vmin=0, vmax=1, cmap=color_palette, cbar=False, ax=ax)

    ax.set_title(cond, fontsize=24, y=1.02)
    ax.set_xlabel('')

    step_size=10
    for i in range(0, 71, step_size):
        ax.axvline(i, color='white', linewidth=5)

    if n<2:
        ax.set_xticks([])

    else:
        my_ticks = this_df.index.get_level_values(1)
        ax.set_xticks(np.arange(len(my_ticks))+0.5, my_ticks)

        x_str = ''
        n_empty = 35

        for i in this_df.index.get_level_values(0)[::step_size]:
            x_str += (str(i) + ' '*n_empty)[:n_empty]

        x_str = x_str.strip()
        ax.set_xlabel(x_str, y=-1)
    
    return ax


# In[11]:


conds = ans_df.columns.levels[0]


# In[12]:


fig = plt.figure(figsize=(30, 22), dpi=300)

for n, cond in enumerate(conds):
    ax = plt.subplot(len(conds)*2, 1, n+1)
    make_ax(ans_df, n, ax)

plt.savefig('../reports/figures/ans_indi_fig.png', bbox_inches='tight')
plt.show()


# In[13]:


fig = plt.figure(figsize=(30, 22))

for n, cond in enumerate(conds):
    ax = plt.subplot(len(conds)*2, 1, n+1)
    make_ax(pred_df, n, ax)

plt.savefig('../reports/figures/pred_indi_fig.png', bbox_inches='tight')
plt.show()

