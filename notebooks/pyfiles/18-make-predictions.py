#!/usr/bin/env python
# coding: utf-8

# # Predictions from Click Patterns

# ### import modules

# In[1]:


import pickle

import glob
import re

import numpy as np
import pandas as pd
from sklearn import metrics

import matplotlib.pyplot as plt
import seaborn as sns

from tqdm.notebook import tqdm


# ### get data

# In[2]:


cond_df = pd.read_csv('../data/interim/lofi_Lower_CUTUP_flat_df.tsv', sep='\t', index_col=[0, 1, 2, 3])


# In[3]:


cond_df


# Number of rows and columns?

# In[4]:


cond_df.shape


# How many million datapoints?

# In[5]:


cond_df.shape[0] * cond_df.shape[1] / 1000000


# ### example participant

# In[6]:


target_p = 'p002'


# #### get participant data

# The ```p_df``` are the data of the selected participant, the ```mean_df``` are the data of all other participants, averaged. We can now try to use the group average as a template and try to compare the different conditions of the target (held-out) participant with the group average by correlations, with the participant data being assigned the label of the group condition with which the correlate the most (winner-take-all).

# In[7]:


p_df = cond_df.xs(target_p, level=3)
p_df.tail()


# #### get all other data

# In[8]:


template_df = cond_df.drop(target_p, level=3)
mean_df = template_df.groupby(level=[0, 1]).mean()
mean_df.tail()


# In[9]:


mean_df.max().max()


# ### correlate group data with held-out participant

# In[10]:


def make_corr_df(p_df, mean_df):
    
    corr_dict = {}
    
    for i in tqdm(p_df.index, leave=False):
        
        this_series = p_df.loc[i, :]
        this_corr = mean_df.T.corrwith(this_series)
        corr_dict[i] = this_corr
        
    p_corr_df = pd.DataFrame(corr_dict).T
    
    return p_corr_df


# In[11]:


p_corr_df = make_corr_df(p_df, mean_df)


# In[17]:


p_corr_df.loc['Surprised']


# ### use labels of highest correlation as winner-take-all prediction

# In[12]:


def make_pred_df(p_corr_df, im_pred=False):
    
    pred_df = pd.DataFrame(p_corr_df.idxmax(axis=1))
    pred_df.columns = ['predicted']
    
    if im_pred:
        pred_df.loc[:, 'true'] = [i for i in pred_df.index]      
        
    else:
        pred_df.loc[:, 'true'] = [(i[0], i[1]) for i in pred_df.index]
    
    return pred_df


# In[13]:


pred_df = make_pred_df(p_corr_df)
pred_df


# In[15]:


pred_df.loc['Surprised']


# ### make held-out WTA predictions for all participants

# In[14]:


def make_big_pred_df(cond_df, im_pred=False, my_level=3):
    
    big_pred_df = pd.DataFrame()

    for target_p in tqdm(cond_df.index.levels[my_level], leave=False):

        # get participant
        p_df = cond_df.xs(target_p, level=my_level)

        # get average of all others
        template_df = cond_df.drop(target_p, level=my_level)
        
        # this has to be adjusted when we use images instead of real answers
        if my_level == 1:    
            mean_df = template_df.groupby(level=[0]).mean()
        else:
            mean_df = template_df.groupby(level=[0, 1]).mean()
            
        # correlate
        p_corr_df = make_corr_df(p_df, mean_df)

        # predict based on biggest correlation
        pred_df = make_pred_df(p_corr_df, im_pred=im_pred)
        
        # reshape
        pred_df = pred_df.drop('true', axis=1).T # true is redundant???
        pred_df.index = [target_p]
        
        big_pred_df = pd.concat([big_pred_df, pred_df])
        
    return big_pred_df


# In[15]:


big_pred_df = make_big_pred_df(cond_df)


# In[16]:


big_pred_df.tail()


# ### do this for all masking conditions

# In[17]:


big_pred_df = pd.DataFrame()

for cond in tqdm(['Full', 'Lower', 'Upper']):

    file = f'../data/interim/lofi_{cond}_CUTUP_flat_df.tsv'
    cond_df = pd.read_csv(file, sep='\t', index_col=[0, 1, 2, 3])
    
    cond_pred_df = make_big_pred_df(cond_df)
    cond_pred_df.columns = pd.MultiIndex.from_tuples([(cond, c[0], c[1], c[2]) for c in cond_pred_df.columns])
    
    big_pred_df = pd.concat([big_pred_df, cond_pred_df], axis=1)


# In[18]:


big_pred_df


# #### store

# In[19]:


big_pred_df.to_csv('../data/processed/big_pred_CUTUP_df.tsv', sep='\t')


# ## Make accuracies

# ### for predicting true image label from click pattern

# In[20]:


def make_acc_df(big_pred_df, pos):    
    
    im_ans_df = big_pred_df.applymap(lambda x:x[pos] if x==x else x)
    acc_df = im_ans_df==im_ans_df.apply(lambda x:x.name[pos])
    acc_df = acc_df.groupby(level=[0, 1], axis=1).sum()
    
    return acc_df


# In[21]:


im_acc_df = make_acc_df(big_pred_df.loc[:, 'Full'], 0)
im_acc_df.tail()


# ### do this for all masking conditions

# In[22]:


def make_big_acc_df(big_pred_df, pos):

    big_acc_df = pd.DataFrame()

    for c in big_pred_df.columns.levels[0]:

        this_pred_df = big_pred_df.loc[:, c]
        this_acc_df = make_acc_df(this_pred_df, pos)
        
        this_acc_df.columns = pd.MultiIndex.from_tuples([(c, i[0], i[1]) for i in this_acc_df.columns])

        big_acc_df = pd.concat([big_acc_df, this_acc_df], axis=1)

    big_acc_df = big_acc_df/10
    
    return big_acc_df


# ### For images

# In[23]:


big_im_acc_df = make_big_acc_df(big_pred_df, 0)
big_im_acc_df.tail()


# In[24]:


plt.figure(figsize=(16, 6))
sns.heatmap( big_im_acc_df.loc[:, 'Full'], cmap='Reds' )
plt.show()


# #### store for re-use

# In[25]:


big_im_acc_df.to_csv('../data/processed/big_im_CUTUP_acc_df.tsv', sep='\t')


# ### For predicting participants' answers (instead of true image labels)

# In[26]:


big_ans_acc_df = make_big_acc_df(big_pred_df, 1)
big_ans_acc_df.tail()


# In[27]:


big_ans_acc_df = make_big_acc_df(big_pred_df, 1)
big_ans_acc_df.tail()


# In[28]:


big_ans_acc_df.to_csv('../data/processed/big_ans_CUTUP_acc_df.tsv', sep='\t')


# ## With Pixel Values of Images

# ### make predictions

# In[29]:


def make_ims_pred_df(file_name):

    img_df = pd.read_csv(file_name, sep='\t', index_col=[0, 1, 2])
    conds = img_df.index.levels[0]

    biggest_pred_df = pd.DataFrame()
    
    for cond in tqdm(conds):
        
        full_df = img_df.loc[cond, :]
        
        big_pred_df = make_big_pred_df(full_df, im_pred=True, my_level=1)
        big_pred_df.columns = pd.MultiIndex.from_tuples([(cond, i) for i in big_pred_df.columns])

        biggest_pred_df = pd.concat([biggest_pred_df, big_pred_df], axis=1)

    biggest_pred_df = biggest_pred_df.sort_index(axis=1)
    
    return biggest_pred_df


# ### for all masking conditions

# There are three maskings:
# 
# * lofi: no masking but downsampled to deal with memory limitations
# * cropped: square mask
# * masked: only pixels into which participants clicked

# In[30]:


file_list = glob.glob('../data/interim/img_*_df.tsv')
file_list


# In[31]:


im_pred_df = pd.DataFrame()

regex = re.compile(r'.*img_(\w+)_df.*')
for file in tqdm(file_list):

    mask_name = regex.findall(file)[-1]
    mask_name = 'Image ' + mask_name[0].upper() + mask_name[1:]
    
    this_pred_df = make_ims_pred_df(file)
    this_pred_df.columns = pd.MultiIndex.from_tuples([(mask_name, i[0], i[1]) for i in this_pred_df.columns])

    im_pred_df = pd.concat([im_pred_df, this_pred_df], axis=1)


# In[32]:


im_pred_df.tail()


# In[33]:


im_pred_df.to_csv('../data/processed/im_pred_df.tsv', sep='\t')


# ### accuracies for image-based predictions

# In[34]:


im_acc_df = im_pred_df==im_pred_df.apply(lambda x:x.name[2])
im_acc_df.tail()


# In[35]:


im_acc_df.to_csv('../data/processed/im_acc_df.tsv', sep='\t')


# ## Combine Tables

# #### click patterns for true image

# In[36]:


click_im_acc_df = big_im_acc_df.groupby(level=[0, 1], axis=1).sum()
click_im_acc_df.columns = pd.MultiIndex.from_tuples([('Click-Im', i[0], i[1]) for i in click_im_acc_df.columns])
click_im_acc_df.tail()


# #### click patterns for answers

# In[37]:


click_ans_acc_df = big_ans_acc_df.groupby(level=[0, 1], axis=1).sum()
click_ans_acc_df.columns = pd.MultiIndex.from_tuples([('Click-Ans', i[0], i[1]) for i in click_ans_acc_df.columns])
click_ans_acc_df.tail()


# ### other data

# #### emotion label answers

# In[38]:


ans_df = pd.read_csv('../data/processed/big_correct_df.tsv', sep='\t', index_col=[0], header=[0, 1])
ans_df.columns = pd.MultiIndex.from_tuples([('Answer', i[0], i[1]) for i in ans_df.columns])
ans_df.tail()


# #### image predictions

# In[39]:


im_acc_df.tail()


# ### combine

# In[40]:


merge_df = pd.concat([ans_df, click_im_acc_df, click_ans_acc_df, im_acc_df], axis=1)
merge_df.to_csv('../data/processed/acc_merge_df.tsv', sep='\t')

