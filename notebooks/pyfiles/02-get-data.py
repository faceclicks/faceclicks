#!/usr/bin/env python
# coding: utf-8

# # Get Data

# ### import modules

# In[1]:


import re
import glob
import json

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from tqdm.notebook import tqdm


# ### get data

# In[2]:


raw_df = pd.read_csv('../data/raw/raw_data_publish.tsv', sep='\t', index_col=[0], header=[0], low_memory=False)
raw_df.head()


# ### rename index

# In[3]:


start_list = range(1, raw_df.shape[0])
rename_dict = {}
for i in start_list:
    rename_dict[i] = f'p{str(i).zfill(3)}'
raw_df = raw_df.rename(rename_dict)


# In[4]:


raw_df.head()


# ### remove people who dropped out

# In[5]:


p_list = [p for p in raw_df.index if str(p).startswith('p') and raw_df.loc[p,'Progress']=='100']


# In[6]:


df = raw_df.loc[p_list, :]
p_list = list(df.index)


# In[7]:


raw_df.shape, df.shape


# In[8]:


df.iloc[:,1210:]


# ### check order

# Which Masking condition comes first?

# In[9]:


outer_loop_df = df[[c for c in df.columns if c.startswith('FL_116')]].astype(int)


# In[10]:


sns.barplot(data=outer_loop_df)
sns.despine(bottom=True)
plt.show()


# ### Order of trials within each masking condition

# In[11]:


inner_loops_df = df[[c for c in df.columns if c.startswith('FL_117') or c.startswith('FL_118')]].astype(int).T
inner_loops_df.index = pd.MultiIndex.from_tuples([x.split('_DO_') for x in inner_loops_df.index])
inner_loops_df = inner_loops_df.sort_index().T


# In[12]:


plt.figure(figsize=(32, 6))
sns.barplot(data=inner_loops_df)
plt.xticks(rotation=90)
sns.despine(bottom=True)
plt.show()


# ### get durations

# In[13]:


durations = df.loc[p_list, 'Duration (in seconds)'].astype(int)/60

#remove extreme outliers
durations = durations[durations<200]

durations = durations.sort_values()

sns.boxplot(data=durations, width=0.2)
sns.swarmplot(data=durations, size=10, alpha=0.4, edgecolor='white', linewidth=1)

for i in durations.index[:3]:
    plt.annotate(xy=(0, durations[i]), text=i)
for i in durations.index[-3:]:
    plt.annotate(xy=(0, durations[i]), text=i)
    
plt.ylabel('Time in Minutes')
#plt.ylim(0,100)
plt.xticks([])
sns.despine(bottom=True)
plt.show()


# In[14]:


durations.mean(), durations.median()


# #### dictionaries for conditions and categories

# In[15]:


with open('../references/cond_dict.json', 'r') as fp:
    cond_dict = json.load(fp)


# In[16]:


cond_dict


# In[17]:


with open('../references/cat_dict.json', 'r') as fp:
    cat_dict = json.load(fp)


# ### get categorical answers

# #### show length of coordinate string as sanity check

# Because storing the click coordinates did not always work, from p028 onwards we stored them twice, to have higher probability of one of the two versions being stored correctly.

# In[18]:


len_dict = {}
for p in p_list:
    len_dict[(p,'1')] = len(df.loc[p, 'all-coords'])
    try:
        len_dict[(p,'2')] = len(df.loc[p, 'all-coords2'])
    except:
        pass
len_df = pd.DataFrame(len_dict, index=['len']).stack(1)
len_df.T['len'].style.bar()


# ### get categorical answers

# In[19]:


def make_ans_df(df, cat_dict=cat_dict):
    
    cond_dict = {'':'Full', '-MO':'Lower', '-MU':'Upper'}
    
    fi = df.loc[:, [c for c in df.columns if c.endswith('Auswahl')]]

    cols = []
    regex = re.compile(r'(\w\w\d\d)(\w\w\w)(.*) Auswahl')
    for c in fi.columns:
        cols.append(regex.findall(c)[-1])
    fi.columns = pd.MultiIndex.from_tuples(cols)

    ff = fi.rename(cond_dict, axis=1).rename(cat_dict,axis=1)
    
    ff =ff.reorder_levels([1,0,2],axis=1).stack([0,1])

    return ff


# In[20]:


ans_df = make_ans_df(df)


# In[21]:


ans_df


# Check number of answers given

# In[22]:


ans_df.groupby(level=0, axis=0).count()


# In[23]:


assert (ans_df.groupby(level=0, axis=0).count() == 70).all().all()


# ### get coordinates

# Each answer as an Id in Qualtrics

# In[24]:


id_to_stim_df = pd.read_csv('../references/stim_table.tsv', sep='\t', header=None, index_col=0)
id_to_stim_dict = id_to_stim_df.to_dict()[1]


# In[25]:


id_to_stim_dict


# In[26]:


def make_coord_df(p_df, id_to_stim_dict, allcoords):
    
    txt = p_df.loc[:,allcoords][-1]

    # CLEAN UP 
    txt = txt.strip('undefined')
    
    d = {}

    this_txt = ''
    for i in txt.split('\n'):
        if i.startswith('QID'):
            this_txt = ''
            key = i
        if not i.startswith('QID') and i !='': 
            this_txt += i
            d[key] = this_txt


    regex = re.compile(r'(xy\d\d):(\d+)\t(\d+)')

    big_df = pd.DataFrame()

    for key in d.keys():

        my_df = pd.DataFrame( regex.findall(d[key]) )
        my_df.columns = ['num', 'x', 'y']
        my_df.index = pd.MultiIndex.from_tuples([(key,i) for i in my_df.index])
        my_df = my_df.drop(['num'], axis=1)

        big_df = pd.concat([big_df, my_df])

    big_df = big_df.rename(id_to_stim_dict).astype(int)
    fu = big_df.unstack(1)
    fa = fu.reorder_levels([1,0],axis=1).sort_index(axis=1)
    d = {}
    for i in fa.index:
        fi = fa.loc[i].dropna()
        fo = fi.unstack(1).T.apply(lambda x:str([x['x'], x['y']]))
        d[i] = [ [tuple(eval(x)) for x in fo] ]
    coords_df = pd.DataFrame(d, index=[p_df.index[-1]])

    return coords_df


# Example participant:

# In[27]:


p_num = p_list[-1]
p_num


# In[28]:


p_df = df.loc[[p_num]].dropna(axis=1)


# ### get coordinate pairs

# In[29]:


coord_df = make_coord_df(p_df, id_to_stim_dict, 'all-coords')
coord_df


# ### merge the two versions, if available

# In[30]:


def make_both_coord_df(p_df, id_to_stim_dict):
     
    coord_df1 = make_coord_df(p_df, id_to_stim_dict, allcoords='all-coords')
    
    try:
        coord_df2 = make_coord_df(p_df, id_to_stim_dict, allcoords='all-coords2')

        overlap_cols = coord_df1.columns[coord_df1.columns == coord_df2.columns]
        assert (coord_df1[overlap_cols] == coord_df2[overlap_cols]).values.all(), 'overlapping columns do not match, concatenation might be invalid!'

        coord_df = pd.concat([coord_df1, coord_df2], axis=1).groupby(level=0, axis=1).first()

    except:
        coord_df = coord_df1.copy()
        
    return coord_df


# In[31]:


coord_df = make_both_coord_df(p_df, id_to_stim_dict)
coord_df


# In[32]:


def make_clean_coord_df(coord_df, cat_dict=cat_dict, id_to_stim_dict=id_to_stim_dict):

    cond_dict = {'':'Full', ' MO':'Lower', ' MU':'Upper'} # has to be slightly different...
    
    cols = []
    regex = re.compile(r'(\w\w\d\d)(\w\w\w)(.*)-coords')
    for c in coord_df.columns:
        cols.append(regex.findall(c)[-1])

    coord_df.columns = pd.MultiIndex.from_tuples(cols)
    coord_df = coord_df.stack([1, 0]).loc[coord_df.index[-1]]
    coord_df = coord_df.rename(cat_dict).rename(cond_dict,axis=1)

    return coord_df


# In[33]:


clean_coord_df = make_clean_coord_df(coord_df)
clean_coord_df


# How many clicks per Category (of 10 images each)?

# In[34]:


clean_coord_df.applymap(lambda x:len(x)).groupby(level=[0]).sum()


# ### collect results

# To split into upper and lower halves, we need the heigth at which the cut was made for each image

# In[35]:


stim_df = pd.read_csv('../data/interim/stim_df.tsv', sep='\t', index_col=[0, 1, 2])
stim_df.tail()


# In[36]:


def make_result_df(cond, clean_coord_df, stim_df = stim_df):

    result_dict = {}
    
    for cat, ident in clean_coord_df.index:

        result_dict[(cat, ident)] = {'upper': 0, 'lower': 0}
        coords = clean_coord_df.loc[(cat, ident), cond]
        cut = stim_df.loc[(cond, cat, ident), 'cut']

        for x,y in coords:
            if y < cut: # because the coordinates of the images are "upside-down", smaller values are higher!
                result_dict[(cat, ident)]['upper'] += 1
            else:
                result_dict[(cat, ident)]['lower'] += 1

    # table with upper/lower counts
    result_df = pd.DataFrame(result_dict)
    
    # add raw data
    result_df = pd.concat([result_df, clean_coord_df.T.loc[[cond]]])
        
    return result_df


# In[37]:


result_df = make_result_df('Full', clean_coord_df)
result_df


# ### add diff

# In[38]:


def add_diff(result_df):
    
    diff_df = pd.DataFrame(result_df.loc['upper'] - result_df.loc['lower']).T
    diff_df.index = ['diff']

    result_df = pd.concat([result_df, diff_df])
    
    return result_df


# In[39]:


result_df = add_diff(result_df)
result_df


# ### add answer
# 
# So we can later distinguish between coordinates for correct and incorrect answers

# In[40]:


def add_ans(p_num, result_df, ans_df):
    
    cond = result_df.index[2]
    
    this_ans_df = ans_df.loc[p_num, [cond]].copy().T

    this_ans_df.index = ['Answer']

    merge_df = pd.concat([result_df, this_ans_df])
    
    return merge_df


# In[41]:


merge_df = add_ans(p_num, result_df, ans_df)
merge_df


# ### do one condition

# In[42]:


def make_cond(cond, p_num, df):
    
    # because the number of columns is determined by the participant with the most clicks,
    # these columns will be nan for other participants and nan columns lead to exceptions
    # so we have to drop nan columns beforehand
    p_df = df.loc[[p_num]].dropna(axis=1)
    
    coord_df = make_both_coord_df(p_df, id_to_stim_dict)

    clean_coord_df = make_clean_coord_df(coord_df)
    
    result_df = make_result_df(cond, clean_coord_df)
    result_df = add_diff(result_df)
    
    merge_df = add_ans(p_num, result_df, ans_df)
    
    return merge_df


# In[43]:


merge_df = make_cond('Upper', p_num, df)
merge_df


# In[44]:


merge_df = make_cond('Full', p_num, df)
merge_df


# ### do all for one participant

# In[45]:


def make_p(p_num, df, cond_dict=cond_dict):
    
    p_df = pd.DataFrame()
    
    for _ in cond_dict:

        cond = cond_dict[ _ ]
        
        cond_df = make_cond(cond, p_num, df)
        cond_df = cond_df.rename({cond: 'coords'})
        
        cols = [(cond, c[0], c[1]) for c in cond_df.columns]
        cond_df.columns = pd.MultiIndex.from_tuples(cols)
        
        p_df = pd.concat([p_df, cond_df], axis=1)
        
    return p_df


# In[46]:


make_p(p_num, df)


# ### make for all p

# In[47]:


big_df = pd.DataFrame()

for p_num in tqdm(df.index):
    
    try:
        result_df = make_p(p_num, df)

        idx = pd.MultiIndex.from_tuples([(p_num, i) for i in result_df.index])
        result_df.index = idx

        big_df = pd.concat([big_df, result_df])
        
    except:
        print(p_num, '!')


# In[48]:


big_df.index.levels[0]


# #### how many participants?

# In[49]:


big_df.index.levels[0].shape


# ### rename data into english

# dictionary with german-english recoding

# In[50]:


rename_dict = {'Angst': 'Fearful', 
               'Ekel': 'Disgusted',
               'Freude': 'Happy',
               'Neutral': 'Neutral',
               'Trauer':'Sad', 
               'Ärger': 'Angry',
               'Überraschung': 'Surprised'}


# function to rename only cells which contain strings which are keys in the above dictionary

# In[51]:


def make_rename(x, rename_dict=rename_dict):
    
    if type(x) == str:
        
        try:
            return rename_dict[x]
        
        except:
            return eval(x)
        
    else:
        return x


# rename cell entries into english

# In[52]:


big_df = big_df.applymap(lambda x:make_rename(x))


# In[53]:


big_df.tail()


# ### without coordinates

# In[54]:


clean_big_df = big_df.drop('coords', level=1)
clean_big_df = clean_big_df.unstack(1).reorder_levels([0, 3, 1, 2], axis=1).sort_index(axis=1)


# In[55]:


clean_big_df.tail()


# ### store data

# In[56]:


big_df.to_csv('../data/interim/big_df.tsv', sep='\t')


# In[57]:


clean_big_df.to_csv('../data/interim/clean_big_df.tsv', sep='\t')


# ### example re-load

# with coordinates

# In[58]:


pd.read_csv('../data/interim/big_df.tsv', sep='\t', index_col=[0, 1], header=[0, 1, 2])


# without coordinates

# In[59]:


pd.read_csv('../data/interim/clean_big_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])

