#!/usr/bin/env python
# coding: utf-8

# # Visualize Confusion Image Statistics

# ### import packages

# In[1]:


import pickle
import json

import numpy as np

import pandas as pd
import cv2
from scipy import stats
from skimage import measure

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_context('talk')

from matplotlib.colors import ListedColormap
from matplotlib import animation
from IPython.display import HTML

from tqdm.notebook import tqdm


# ### get settings

# In[2]:


with open('../models/palette.pkl', 'rb') as fp:
    color_palette = pickle.load(fp)
    
with open('../models/primary_palette.pkl', 'rb') as fp:
    primary_palette = pickle.load(fp)
    
with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)

with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# In[3]:


with open('../data/interim/im_dims.json', 'r') as fp:
    dim_dict = json.load(fp)
    
n_rows = dim_dict['n_rows']
n_cols = dim_dict['n_cols']
new_n_rows = dim_dict['new_n_rows']
new_n_cols = dim_dict['new_n_cols']
scale_factor = dim_dict['scale_factor']
im_cut_height = 432 # CAVE HACK # dim_dict['im_cut_height']


# ### get stimuli

# In[4]:


stim_df = pd.read_csv('../data/interim/stim_df.tsv', sep='\t', index_col=[0, 1, 2])
stim_df.tail()


# In[5]:


def load_im(file_name):
    
    img = cv2.imread(file_name,  cv2.IMREAD_GRAYSCALE)
    x_shape, y_shape = img.shape
    scale = 1
    new_shape = (int(y_shape*scale), int(x_shape*scale))
    res = cv2.resize(img, dsize=new_shape)
    
    return res


# ### average face

# In[6]:


biggest_im = np.zeros((n_rows, n_cols))[::scale_factor, ::scale_factor]
this_stim_df = stim_df.loc['Full']
for i in this_stim_df['file_name']:
    im_name = load_im(i)[::scale_factor, ::scale_factor]
    biggest_im += im_name


# In[7]:


np.savetxt('../data/interim/biggest_im.txt', biggest_im)


# ### get height of dividing line

# In[8]:


orig_height = load_im(stim_df.iloc[0,0]).shape[0]
cuts = stim_df.loc[:, 'cut'].mean()
cut_ratio = cuts/orig_height
cut_ratio


# In[9]:


im_cut_height = int(biggest_im[30:-30,20:-20].shape[0]*cut_ratio)
im_cut_thick = int(stim_df.loc[:, 'cut'].std())
im_cut_height, im_cut_thick


# ### get data

# In[10]:


with open('../data/processed/stat_ims.json', 'rb') as fp:
    thresh_dict = pickle.load(fp)


# In[11]:


with open('../data/processed/lothresh_stat_ims.json', 'rb') as fp:
    unthresh_dict = pickle.load(fp)


# ### example plot with contours

# contours: https://scikit-image.org/docs/stable/auto_examples/edges/plot_contours.html

# In[12]:


unthresh_im = unthresh_dict['Full']['clean']['Happy-Happy']
thresh_im = thresh_dict['Full']['clean']['Happy-Happy']


# ### big plot

# In[13]:


def make_big_plot(cond, comparison, fig, unthresh_dict=unthresh_dict, thresh_dict=thresh_dict, settings_dict=settings_dict, im_cut_height=im_cut_height):
    
    my_max = 0.7#stat_df.xs('mean', level=2).max().max()
    
    # get sample size information (this is clumsy)
    if cond == 'Full':
        n_df = pd.read_csv(f'../data/processed/stat_{cond}_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
    elif cond == 'Half':
        n_upper_df = pd.read_csv(f'../data/processed/stat_Upper_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
        n_lower_df = pd.read_csv(f'../data/processed/stat_Lower_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
    else:
        n_upper_df = pd.read_csv(f'../data/processed/stat_Full_gr_Upper_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
        n_lower_df = pd.read_csv(f'../data/processed/stat_Full_gr_Lower_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
        
    for i, emo in enumerate(my_order):

        for n, c in enumerate( my_order ):

            ax = plt.subplot(len(my_order)+1, len(my_order), i*len(my_order) + n+1)
            
            try:
                stat_im = unthresh_dict[cond][comparison][f'{emo}-{c}']
                contour_im = thresh_dict[cond][comparison][f'{emo}-{c}']
                
                if cond == 'Full':
                    this_n = n_df.loc[(emo, c, 'sample n'), '0']
                    sample_size = f'n={this_n:.0f}'
                else:
                    this_upper_n = n_upper_df.loc[(emo, c, 'sample n'), '0']
                    this_lower_n = n_lower_df.loc[(emo, c, 'sample n'), '0']
                    sample_size = f'n={this_upper_n:.0f}/{this_lower_n:.0f}'

            except:
                stat_im = np.zeros((new_n_rows, new_n_cols)) * np.nan
                contour_im = np.zeros((new_n_rows, new_n_cols)) * np.nan
                sample_size = f'n=0'
            
            bg_min, bg_max =biggest_im.min(), biggest_im.max()
            ax.imshow(biggest_im[30:-30,20:-20], cmap='Greys_r', alpha=0.7, vmin=bg_min, vmax=bg_max)
            ax.imshow(stat_im[30:-30,20:-20], 
                      vmin=-my_max, 
                      vmax=my_max, 
                      cmap=ListedColormap(color_palette)
                      )
            
            # sample size
            ax.set_xlabel(sample_size, fontsize=12)
            
            # Find contours at a constant value of 0.8
            contour_im = contour_im[30:-30,20:-20]
            contours = measure.find_contours(abs(contour_im)>0, 0.9999)
            for contour in contours:
                ax.plot(contour[:, 1], contour[:, 0], linewidth=2, color='k')
            
            if i != n:
                sns.despine(bottom=True, left=True, ax=ax)
                
            ax.set_xticks([]); ax.set_yticks([]);
            
            if i==0:
                ax.set_title(c, fontsize=16, y=1)

            if n==0:
                my_title = ((' '*5) + emo)[-9:]
                ax.set_ylabel(my_title, rotation=0, fontsize=16, labelpad=45)

            if cond != 'Full':
                ax.axhline(im_cut_height, color='k', linewidth=3, alpha=1)

    plt.tight_layout()
    return fig


# ### Conditions

# In[14]:


conds = ['Full', 'Half', 'Full_gr_Half']


# ### Baseline

# In[15]:


for cond in conds:

    fig = plt.figure(figsize=(14, 19), dpi=300)
    fig = make_big_plot(cond, 'baseline', fig)
    plt.suptitle(cond, y=1.05)
    plt.savefig(f'../reports/figures/im_stats_{cond}_fig.png', bbox_inches='tight')
    plt.show()


# ### Clean

# In[16]:


for cond in conds:

    fig = plt.figure(figsize=(14, 19), dpi=300)
    fig = make_big_plot(cond, 'clean', fig)
    plt.suptitle(cond, y=1.05)
    plt.savefig(f'../reports/figures/im_confusion_clean_{cond}_fig.png', bbox_inches='tight')
    plt.show()


# In[17]:


fig = plt.figure(figsize=(16, 18), dpi=300)

for n, cond in enumerate(['Full', 'Half']):

    im_file = f'../reports/figures/im_confusion_clean_{cond}_fig.png'
    im = plt.imread(im_file)

    ax = plt.subplot(1, 2, n+1)
    ax.imshow(im)

    ax.set_axis_off()

plt.tight_layout()
plt.show()


# ## Confusions Difference to Correct

# In[18]:


fig = plt.figure(figsize=(14, 19), dpi=300)
fig = make_big_plot('Full', 'confusions', fig)
plt.savefig(f'../reports/figures/im_stats_confusions_fig.png', bbox_inches='tight')
plt.show()


# ## Compare click patterns
# 
# 
# Is a click pattern more similar to click patterns for the same expression or to click patterns for the same answer?

# ### get data averaged by image id

# In[19]:


conds = ['Full', 'Upper', 'Lower']
comparisions = ['baseline', 'clean']


# In[20]:


mean_dict = {}
for comparison in tqdm(comparisions):
    for cond in tqdm(conds, leave=False):
        
        this_big_flat_df = pd.read_csv(f'../data/processed/big_{cond}_{comparison}_df.tsv', sep='\t', index_col=[0, 1, 2])
        mean_df = this_big_flat_df.groupby(level=[0, 1]).mean()
        
        mean_dict[(comparison, cond)] = mean_df


# In[21]:


def make_missing(df, my_order=my_order):
    
    copy_df = df.copy()   
    
    for i in my_order:
        this_copy_df = copy_df.loc[i, :]
        
        for j in my_order:
            
            if j not in this_copy_df.index:
                copy_df.loc[(i, j), :] = np.nan
                copy_df.loc[:, (i, j)] = np.nan
                
    copy_df = copy_df.sort_index(axis=0)
    copy_df = copy_df.sort_index(axis=1)

    copy_df = copy_df.loc[(my_order, my_order), (my_order, my_order)]
    
    return copy_df


# In[22]:


corr_dict = {}
for key in tqdm(mean_dict.keys()):
    mean_df = mean_dict[key]
    corr_df = mean_df.T.corr()
    corr_clean_df = make_missing(corr_df)
    corr_dict[key] = corr_clean_df


# In[23]:


corr_clean_df = corr_dict[('clean', 'Upper')]


# In[24]:


plt.figure(figsize=(52, 28), dpi=100)

for n, cond in enumerate(conds):
    
    ax = plt.subplot(1, 3, n+1)
    
    corr_clean_df = corr_dict[('clean', cond)]

    sns.heatmap(corr_clean_df, square=True, vmin=-1, vmax=1, cmap='RdYlBu_r', cbar=False, ax=ax)

    for i in range(0,7*7+1, 7):
        ax.axvline(i, c='k', linewidth=4)
        ax.axhline(i, c='k', linewidth=4)
        
    if n > 0:
        ax.set_yticks([])
    
    ax.set_ylabel(''), ax.set_xlabel('')
        
    ax.set_title(cond)
    
plt.show()


# In[25]:


def make_ans_df(corr_clean_df):
    
    copy_df = corr_clean_df.copy()
    
    copy_df = copy_df.reorder_levels([1, 0], axis=0)
    copy_df = copy_df.reorder_levels([1, 0], axis=1)
    
    copy_df = copy_df.sort_index(axis=0)
    copy_df = copy_df.sort_index(axis=1)

    copy_df = copy_df.loc[(my_order, my_order), (my_order, my_order)]
    
    return copy_df


# In[26]:


plt.figure(figsize=(52, 28), dpi=100)

for n, cond in enumerate(conds):
    
    ax = plt.subplot(1, 3, n+1)
    
    corr_clean_df = corr_dict[('clean', cond)]
    corr_ans_df = make_ans_df(corr_clean_df)
    sns.heatmap(corr_ans_df, square=True, vmin=-1, vmax=1, cmap='RdYlBu_r', cbar=False, ax=ax)

    for i in range(0,7*7+1, 7):
        ax.axvline(i, c='k', linewidth=4)
        ax.axhline(i, c='k', linewidth=4)
        
    if n > 0:
        ax.set_yticks([])
    
    ax.set_ylabel(''), ax.set_xlabel('')
        
    ax.set_title(cond)
    
plt.show()


# ### plot

# In[27]:


palette3 = [primary_palette[2], primary_palette[3], primary_palette[0], primary_palette[1]]


# In[28]:


def make_max_df(corr_clean_df):
    
    fu = corr_clean_df.round(11).replace(1.0, np.nan)
    
    #### Make sure there are not ties
    assert (((fu.apply(lambda x:x==fu.max())).sum(axis=1))<=1).all()
    #### Make sure it does not matter whether we go by rows or columns (should be symmetrical)
    assert (fu.idxmax(axis=0).fillna('FFFF') == fu.idxmax(axis=1).fillna('FFFF')).all()

    fi = pd.DataFrame(fu.idxmax())
    
    return fi


# In[29]:


max_df = make_max_df(corr_clean_df)
max_df.tail(7)


# In[30]:


def make_hit_df(fi):
    
    fx = fi.applymap(lambda x:x[0] if x==x else x)
    fx.columns = ['true']
    fx.loc[:, 'ans'] = fi.applymap(lambda x:x[1] if x==x else x)

    fx.loc[:, 'true_hit'] = fx.index.get_level_values(0) == fx.loc[:, 'true']
    fx.loc[:, 'ans_hit'] = fx.index.get_level_values(1) == fx.loc[:, 'ans']

    aa = (fx.loc[:, 'true'] != fx.loc[:, 'true']).astype(int) + (fx.loc[:, 'ans'] != fx.loc[:, 'ans']).astype(int)
    fx.loc[:, 'both_nan'] = aa > 1
    fff = fx.iloc[:, 2:]

    return fff


# In[31]:


hit_df = make_hit_df(max_df)
hit_df.tail(7)


# In[32]:


def make_cm_df(fff, my_order=my_order):
    
    copy_df = fff.copy()

    copy_df.loc[:, 'true_hit'] = fff.loc[:, 'true_hit'] * 3
    copy_df.loc[:, 'ans_hit'] = fff.loc[:, 'ans_hit'] * 2
    copy_df.loc[:, 'both_nan'] = fff.loc[:, 'both_nan'] * 1
    #### make sure there are no overlaps
    assert (copy_df.sum(axis=1) == copy_df.max(axis=1)).all()

    cm = copy_df.sum(axis=1).unstack().loc[my_order, my_order]

    return cm


# In[33]:


cm_df = make_cm_df(hit_df)
cm_df


# In[34]:


def make_annot(fi):
    bb = fi.unstack()[0].loc[my_order, my_order].astype(str)
    bb = bb.applymap(lambda x:'\n'.join(x.split(',')).strip('(').strip(')').replace("'", "").replace(' ', ''))
    bb = bb.replace('nan', 'N.A.')
    return bb


# In[35]:


make_annot(max_df)


# In[36]:


plt.figure(figsize=(22, 6), dpi=100)

for n, cond in enumerate(conds):
    print(cond)
    ax = plt.subplot(1, 3, n+1)
    
    corr_clean_df = corr_dict[('clean', cond)]
    
    max_df = make_max_df(corr_clean_df)
    hit_df = make_hit_df(max_df)
    cm_df = make_cm_df(hit_df)
    annot_df = make_annot(max_df)
    
    sns.heatmap(cm_df,
                annot=annot_df,
                fmt='s',
                annot_kws={'fontsize':10},
                square=False,
                cbar=False,
                cmap=palette3)
    
    if n > 0:
        ax.set_yticks([])
    
    ax.set_ylabel(''), ax.set_xlabel('')
        
    ax.set_title(cond)

plt.tight_layout()
plt.savefig('../reports/figures/click_confuse_cm.png', dpi=300, bbox_inches='tight')
plt.show()


# ### Make Legend

# In[37]:


recode_dict = {1: 'N.A.',
               0: 'Neither',
               2: 'Answer',
               3: 'Image'}


# In[38]:


plt.figure()
for c in [3, 2, 0]:
    plt.plot([0,1], [0, 1], 'o', c=palette3[c], label=recode_dict[c])
plt.legend(markerscale=2, edgecolor='k')
plt.savefig('../reports/figures/conf_legend.png', dpi=300)
plt.close()

fig, ax = plt.subplots()
cut_legend = plt.imread('../reports/figures/conf_legend.png')[190:480, 300:750]
ax.imshow(cut_legend)
ax.set_axis_off()

plt.savefig('../reports/figures/conf_legend.png', dpi=300)
plt.show()


# #### merge

# In[39]:


fig = plt.figure(figsize=(16, 6), dpi=300)

gs = fig.add_gridspec(nrows=1, ncols=10, 
                      hspace=0., wspace=0.0)

ax1 = fig.add_subplot(gs[:, :-1])
ax1.imshow(plt.imread('../reports/figures/click_confuse_cm.png'))
ax2 = fig.add_subplot(gs[:, -1])
ax2.imshow(plt.imread('../reports/figures/conf_legend.png'))

ax1.axis('off')
ax2.axis('off')

plt.show()

