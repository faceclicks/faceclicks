#!/usr/bin/env python
# coding: utf-8

# # Growing Sample Plots
# 
# Make plots that show progression of main results as sample grows

# ### import modules

# In[1]:


import json
import pickle

import pandas as pd
from scipy import stats

import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import seaborn as sns
sns.set_context('talk')

from matplotlib import animation
from IPython.display import HTML

from tqdm.notebook import tqdm


# ### get settings

# In[2]:


with open('../models/palette7.pkl','rb') as fp:
    palette7 = pickle.load(fp)
    
with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

my_order = settings_dict['my_order']
my_boot = settings_dict['boots']
my_ci = settings_dict['ci']


# ## get data

# ### clicks

# In[3]:


click_diff_correct_df = pd.read_csv('../data/processed/click_diff_correct_df.tsv', sep='\t', index_col=[0])
click_diff_correct_df = click_diff_correct_df.loc[:, my_order]
click_diff_correct_df.tail()


# ### answers

# In[4]:


ans_diff_df = pd.read_csv('../data/processed/ans_diff_correct_df.tsv', sep='\t', index_col=[0], header=[0])
ans_diff_df = ans_diff_df.loc[:, my_order]
ans_diff_df.tail()


# ### how do the data change as the sample grows?

# get participant numbers as integers

# In[5]:


def make_num_df(df):
    
    num_df = df.copy()
    
    num_df.loc[:, 'p_num'] = num_df.index
    num_df = num_df.melt(id_vars=['p_num'])
    num_df['num'] = num_df['p_num'].apply(lambda x:int(x.strip('p')))
    
    return num_df


# copy participants as many times as the number of slices they belong to (i.e. first participant belongs to all slices, last participant only to one slice)

# In[6]:


def make_slice_df(num_df):

    nums = sorted(set(num_df['num']))

    slice_df = pd.DataFrame()

    for n, i in enumerate(nums):

        idx = num_df[num_df['num']<=i].index
        this = num_df.loc[idx,:]
        this.loc[:,'slice'] = n+1
        slice_df = pd.concat([slice_df, this])

        slice_df.index = range(slice_df.shape[0])
    
    assert list(range(1, len(set(slice_df['slice']))+1)) == list(set(slice_df['slice'])), 'non-continuous numbers'
    
    return slice_df


# ### group data into slices

# In[7]:


num_df = make_num_df(click_diff_correct_df)
click_slice_df = make_slice_df(num_df)
click_slice_df.tail()


# In[8]:


num_df = make_num_df(ans_diff_df)
ans_slice_df = make_slice_df(num_df)
ans_slice_df.tail()


# ## Visualize

# ### make progression plot

# In[9]:


def make_ax(slice_df, ax, my_boot=my_boot, palette7=palette7):
    
    ax = sns.lineplot(data=slice_df, 
                 x='slice', 
                 y='value', 
                 hue='variable', 
                 ci=settings_dict['ci'], 
                 n_boot=my_boot,
                 palette=palette7,
                 hue_order=my_order,
                 ax=ax)

    mean_vals = slice_df.groupby('variable').mean()['value'].sort_values()
    
    for j in mean_vals.index:
        ax.annotate(xy=(slice_df['slice'].max()+0.5, mean_vals[j]), fontsize=12, text=j )

    ax.axhline(0, color='k', linestyle=':', linewidth=2)
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

    ax.get_legend().remove()
    sns.despine(trim=True)

    return ax


# ### combine into one figure

# In[10]:


fig  = plt.figure(figsize=(16, 20))

ax = plt.subplot(2, 1, 1)
ax = make_ax(ans_slice_df, ax)
ax.set_title('Answers', y=0.9, fontsize=28)
ax.set_ylabel('Upper-Lower')

ax = plt.subplot(2, 1, 2)
ax = make_ax(click_slice_df, ax)
ax.set_title('Clicks', y=0.9, fontsize=28)
ax.set_ylabel('Upper-Lower')
ax.set_xlabel('')

plt.tight_layout()
plt.savefig('../reports/figures/sample_growth_fig.png', bbox_inches='tight', dpi=300)
plt.show()

