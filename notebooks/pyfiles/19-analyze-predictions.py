#!/usr/bin/env python
# coding: utf-8

# # Prediction Analyses

# ### import modules

# In[1]:


import json
import pickle

import numpy as np
import pandas as pd

from scipy import stats
from sklearn import metrics

import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import seaborn as sns
sns.set_context('talk')

from matplotlib.offsetbox import OffsetImage, AnnotationBbox


# ### settings

# In[2]:


with open('../references/my_settings.json', 'r') as fp:
    settings_dict = json.load(fp)

chance_rate = settings_dict['guess']
p_five_sigma = settings_dict['p']
    
with open('../models/primary_palette.pkl', 'rb') as fp:
    primary_palette = pickle.load(fp)

palette3 = primary_palette[:3]
my_boot = settings_dict['boots']
my_guess = settings_dict['guess']
my_order = settings_dict['my_order']


# ## Click Predictions

# In[3]:


click_acc_df = pd.read_csv('../data/processed/big_im_CUTUP_acc_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2])
click_acc_df = click_acc_df.groupby(level=[0, 1], axis=1).sum()
click_acc_df.tail()


# ### Barplot with different Emos, like Answers

# In[4]:


long_correct_df = pd.melt(click_acc_df.reset_index(), 
                          id_vars='index', 
                          var_name=['cond', 'emotion'], 
                          value_name='% correct predictions')
long_correct_df


# In[5]:


with open('../models/average_im_dict.pkl', 'rb') as fp:
    avg_im_dict = pickle.load(fp)


# In[6]:


fig, ax = plt.subplots(1,1, figsize=(16, 6))

sns.barplot(data=long_correct_df, 
            x='emotion', 
            y='% correct predictions', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=0,
            ax=ax)

# add jitter to plot swarms
jitter_df = long_correct_df.copy()
jitter_df.loc[:, ['% correct predictions']] = jitter_df.loc[:, ['% correct predictions']].applymap(lambda x:x + (np.random.rand()-0.5)*0.02)

sns.swarmplot(data=jitter_df, 
            x='emotion', 
            y='% correct predictions', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=2,
            alpha=0.8,
            dodge=True,
            zorder=2,
            ax=ax)

sns.barplot(data=long_correct_df, 
            x='emotion', 
            y='% correct predictions', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=my_boot, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

nconds = len(set(long_correct_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)
plt.xticks(y=-0.05)

plt.axhline(my_guess, linewidth=2, linestyle=':', color='k', zorder=0)

# add pictures
for i in ax.get_xticklabels():
    key, (x, y) = i.get_text(), i.get_position()
    im = avg_im_dict[key]
    imagebox = OffsetImage(np.asarray(im), zoom=.15)
    ab = AnnotationBbox(imagebox, (x, y+0.04), pad=0, zorder=1)
    ax.add_artist(ab)

ax.set_xticklabels([i.get_text().upper()[:3] for i in ax.get_xticklabels()], fontsize=22)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
plt.savefig('../reports/figures/pred_acc_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# ### Confusion Matrices

# In[7]:


big_pred_df = pd.read_csv('../data/processed/big_pred_CUTUP_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2, 3])
big_pred_df = big_pred_df.applymap(lambda x:eval(x) if x==x else x)
big_pred_df = big_pred_df.groupby(level=[0, 2, 3], axis=1).first()


# In[8]:


ans_df = big_pred_df.applymap(lambda x:x[1] if x!=None else np.nan)
ans_df = ans_df.fillna('NoAns')
ans_df.tail()


# In[9]:


def make_cm_df(cond, p_num, data_df, my_order=my_order):
    
    y_pred = data_df.loc[p_num, cond]
    y_true = data_df.loc[:, cond].apply(lambda x:x.name[0])

    cm = metrics.confusion_matrix(y_true, y_pred, labels=my_order)
    
    cm_df = pd.DataFrame(cm, index=my_order, columns=my_order)
    cm_df.index.name = 'True'
    cm_df.columns.name = 'Answer'
    
    return cm_df


# In[10]:


make_cm_df('Full', 'p003', ans_df)


# In[11]:


def make_p_cm_df(p_num, ans_df):
    
    big_cm_df = pd.DataFrame()
    
    for cond in ans_df.columns.levels[0]:
        
        cond_cm_df = make_cm_df(cond, p_num, ans_df)
        cond_cm_df.columns = pd.MultiIndex.from_tuples([(cond,c) for c in cond_cm_df.columns])
        
        big_cm_df = pd.concat([big_cm_df, cond_cm_df], axis=1)
    
    big_cm_df = big_cm_df/10
        
    return big_cm_df


# In[12]:


make_p_cm_df('p003', ans_df)


# In[13]:


def make_big_cm_df(ans_df):
    
    big_cm_df = pd.DataFrame()
    
    for p_num in ans_df.index:
        
        sum_df = make_p_cm_df(p_num, ans_df)

        # wide format, with each participant in one row
        sum_df = pd.DataFrame(sum_df.unstack()).T
        sum_df.index = [p_num]
        
        big_cm_df = pd.concat([big_cm_df, sum_df], sort=True)
        
    return big_cm_df


# In[14]:


big_cm_df = make_big_cm_df(ans_df)


# In[15]:


def make_mean_cm_df(big_cm_df):
    
    mean_cm_df = pd.DataFrame(big_cm_df.mean()).unstack(1)[0]
    mean_cm_df = mean_cm_df.unstack(0).reorder_levels([1, 0], axis=1).sort_index(axis=1)
    
    return mean_cm_df


# In[16]:


mean_cm_df = make_mean_cm_df(big_cm_df)


# In[17]:


mean_cm_df


# In[18]:


def make_stat_mask(in_df, chance_rate, p_five_sigma, my_order=my_order):
    
    df = in_df.copy()
    df = df - chance_rate
    
    stat_df = pd.DataFrame(df.apply(lambda x:stats.ttest_1samp(x, 0) if x.std()!=0 else 99), columns=df.columns).T

    mask = pd.DataFrame(stat_df[1]).unstack(0)[1].loc[my_order, my_order] > p_five_sigma
    
    return mask


# In[19]:


def cm_plots(big_df, fig, my_order=my_order, make_mask=False, chance_rate=chance_rate, p_five_sigma=p_five_sigma):
    
    df = make_mean_cm_df(big_df)
    
    conds = df.columns.levels[0]
    for n, c in enumerate(conds):

        cm_df = df.loc[:, c]
        cm_df = cm_df.loc[my_order, my_order]

        if make_mask:
            mask = make_stat_mask(big_df.loc[:, c], chance_rate, p_five_sigma)
        else:
            mask=False
        
        ax = plt.subplot(1, len(conds), n+1)

        sns.heatmap(cm_df, 
                    annot=True, 
                    cmap=blue_palette, 
                    square=True, 
                    cbar=False, 
                    fmt='.0%',
                    alpha=0.3,
                    annot_kws={'alpha':0.3},
                    mask=mask!=mask,
                    ax=ax)
        
        sns.heatmap(cm_df, 
                    annot=True, 
                    cmap=blue_palette, 
                    square=True, 
                    cbar=False, 
                    fmt='.0%',
                    mask=mask,
                    ax=ax)
        
        ax.set_ylabel('')
        ax.tick_params(left=False, bottom=False)
        ax.set_title(c, y=1.05, fontsize=24)
        if n > 0:
            ax.set_yticks([])

    plt.tight_layout()
    #plt.savefig('../reports/figures/ans_cms_fig.png', bbox_inches='tight', dpi=300)

    return fig


# In[20]:


with open('../models/blue_palette.pkl', 'rb') as fp:
    blue_palette = pickle.load(fp)


# In[21]:


fig = plt.figure(figsize=(20, 7), dpi=100)
cm_df = cm_plots(big_cm_df, fig, make_mask=True)
plt.show()


# ## Get aggregate data

# In[22]:


acc_df = pd.read_csv('../data/processed/acc_merge_df.tsv', sep='\t', index_col=[0], header=[0, 1, 2])
acc_df.tail()


# ### average

# In[23]:


mean_df = acc_df.groupby(level=[0, 1], axis=1).mean()


# In[24]:


mean_df.tail()


# ## visualize

# ### predicting true labels or answers given

# In[25]:


diff_pred_df = (mean_df.loc[:, 'Click-Im'] - mean_df.loc[:, 'Click-Ans']).dropna()
diff_pred_df.columns = pd.MultiIndex.from_tuples([('Click-Diff', i) for i in diff_pred_df.columns])


# In[26]:


my_pred_df = pd.concat([mean_df.loc[:, ['Click-Im', 'Click-Ans']].dropna(), diff_pred_df], axis=1)


# In[27]:


my_pred_df = my_pred_df.rename({'Click-Im': 'Predict\nImage',
                   'Click-Ans': 'Predict\nAnswer',
                   'Click-Diff': 'Predict\nImage > Answer'}, axis=1, level=0)


# In[28]:


my_long_df = pd.melt(my_pred_df.reset_index(), 
                     id_vars='index', 
                     var_name=['data', 'cond'], 
                     value_name='% correct')
my_long_df.tail()


# In[29]:


my_order = [ 'Predict\nImage','Predict\nAnswer', 'Predict\nImage > Answer']


# In[30]:


fig, ax = plt.subplots(1, 1, figsize=(9, 6))

sns.barplot(data=my_long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=1,
            ax=ax)

sns.swarmplot(data=my_long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=4,
            alpha=0.8,
            dodge=True,
            zorder=2,
            ax=ax)

sns.barplot(data=my_long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=my_boot, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

#plt.axhline(my_guess, linewidth=2, linestyle=':', color='k')

nconds = len(set(my_long_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

ax.set_xticklabels(ax.get_xticklabels(),fontsize=22)

plt.xlabel('')
plt.show()


# ## comparing all version of true image predictions

# ### reshape into long format

# In[31]:


long_df = pd.melt(mean_df.reset_index(), 
                  id_vars='index', 
                  var_name=['data', 'cond'], 
                  value_name='% correct')
long_df.tail()


# In[32]:


long_df.loc[:, 'data'] = long_df.loc[:, 'data'].apply(lambda x:{'Answer' : 'Accuracy from\nAnswers', 
                                                                'Click-Ans' : 'Click-Ans', 
                                                                'Click-Im' : 'Prediction from\nClick Patterns', 
                                                                'Image Cropped' : 'Prediction from\nCropped Image', 
                                                                'Image Lofi' : 'Prediction from\nWhole Image',
                                                                'Image Masked': 'Prediction from\nMasked Image'}[x])


# ### visualize

# In[33]:


my_order = ['Prediction from\nClick Patterns', 
            'Accuracy from\nAnswers', 
            'Prediction from\nMasked Image',
            'Prediction from\nCropped Image',
            'Prediction from\nWhole Image']


# In[34]:


fig, ax = plt.subplots(1,1, figsize=(16, 6))

sns.barplot(data=long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=0,
            ci=settings_dict['ci'],
            zorder=1,
            ax=ax)

sns.swarmplot(data=long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3,
            linewidth=1,
            edgecolor='white',
            size=4,
            alpha=0.8,
            dodge=True,
            zorder=2,
            clip_on=False,
            ax=ax)

sns.barplot(data=long_df, 
            x='data', 
            y='% correct', 
            hue='cond', 
            order=my_order, 
            palette=palette3, 
            errwidth=5,
            n_boot=my_boot, 
            ci=settings_dict['ci'],
            alpha=0,
            zorder=2,
            ax=ax)

plt.axhline(my_guess, linewidth=2, linestyle=':', color='k')

nconds = len(set(long_df.loc[:, 'cond']))
handles, labels = ax.get_legend_handles_labels()
ax.legend(handles[:nconds], labels[:nconds], loc=(1.01, .3), frameon=False)

sns.despine(bottom=True, trim=True)
ax.tick_params(bottom=False)

ax.set_yticklabels(ax.get_yticklabels(), fontsize=18)
ax.set_xticklabels(ax.get_xticklabels(), fontsize=18)

plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1.0))

plt.xlabel('')
plt.savefig('../reports/figures/pred_compare_fig.png', bbox_inches='tight', dpi=300)
plt.show()


# Here it might be interesting, that we can look at if there is less information in one of the two halves - we see here that while for human ratings the upper half seems overall more important, in the pixel value analyses the lower half does at least as well
