from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Clicking on diagnostic parts of wholes and parts of emotioanal faces',
    author='mwegrzyn',
    license='MIT',
)
